<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteCollectionEntity>
   <description></description>
   <name>Admin Login Flow</name>
   <tag></tag>
   <executionMode>SEQUENTIAL</executionMode>
   <testSuiteRunConfigurations>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Mobile</groupName>
            <profileName>default</profileName>
            <runConfigurationData>
               <entry>
                  <key>kobitonDevice</key>
                  <value>{
  &quot;id&quot;: 82911,
  &quot;udid&quot;: &quot;D1CGAPE750906012&quot;,
  &quot;isBooked&quot;: false,
  &quot;capabilities&quot;: {
    &quot;udid&quot;: &quot;D1CGAPE750906012&quot;,
    &quot;modelName&quot;: &quot;TA-1000&quot;,
    &quot;deviceName&quot;: &quot;Nokia 6&quot;,
    &quot;isEmulator&quot;: false,
    &quot;resolution&quot;: {
      &quot;width&quot;: 1080,
      &quot;height&quot;: 1920
    },
    &quot;platformName&quot;: &quot;Android&quot;,
    &quot;platformVersion&quot;: &quot;7.1.1&quot;,
    &quot;installedBrowsers&quot;: [
      {
        &quot;name&quot;: &quot;chrome&quot;,
        &quot;version&quot;: &quot;68.0.3440.85&quot;
      }
    ]
  },
  &quot;orientation&quot;: &quot;PORTRAIT&quot;,
  &quot;captureScreenShots&quot;: true,
  &quot;isHidden&quot;: false,
  &quot;isOnline&quot;: true,
  &quot;isFavorite&quot;: true,
  &quot;isCloud&quot;: true
}</value>
               </entry>
            </runConfigurationData>
            <runConfigurationId>Kobiton Device</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Admin Login Flow/Admin Login Flow Execution</testSuiteEntity>
      </TestSuiteRunConfiguration>
   </testSuiteRunConfigurations>
</TestSuiteCollectionEntity>
