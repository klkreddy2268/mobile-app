<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Staff login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>fb4326f6-6060-428e-9288-7d8afc1058d2</testSuiteGuid>
   <testCaseLink>
      <guid>6d5e3ca4-8495-402f-816b-95ffa30d8f0f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Login/Verify if able to successfully launch aplication</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>843bce93-ccb1-4100-9638-2df08cadd5a8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Staff Login/Login/Verify if able to click on Remind me later in the update popup</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2138f144-389a-44dc-b14b-9fa48acd3268</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Login/Verify if able to enter school code</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2b40a542-e605-4917-97eb-0127f666f161</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Login/Verify if able to click on proceed after entering school code</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>47f55dd3-244e-4ec3-87ed-862872842e8e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Login/Verify if able to click on Login with Google button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>93def1c4-ab99-4474-9842-7dbf0cc79a8e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Login/Verify if able to select the available login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e8c0f1b1-4170-4931-ac79-d2e7aacfba4c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Login/Verify if able to select Yes in Push Notifcations popup</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
