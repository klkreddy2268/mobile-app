<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Personal Info</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>5784138c-9e7f-4e10-bf9b-ce14469c0d57</testSuiteGuid>
   <testCaseLink>
      <guid>c2f443a2-6e4b-46f4-a65f-2aeb35486867</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Personal Details/Verify successful access of PersonalInfo links</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a3e4c252-3bbf-408f-ac1e-0552b4aa0fa6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Personal Details/Verify Successful access of Details link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f17814c0-8881-4afd-82e2-f02649f1f071</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Personal Details/Verify Successful access of Contact details link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cdca1590-ca4f-4682-b067-58e7f47b0fcf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Personal Details/Verify Successful access of Present Address details link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>168e2a12-338d-4a39-bc15-b3951ac0c5af</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Personal Details/Verify Successful access of Permanent Address details link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>46429891-5177-40a7-b362-08322aea1ed2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Personal Details/Verify back button is clickable or not in Personal deatils</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d2e75e10-a7ba-4d78-8f33-37cbcb62320d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Employment Details/Verify Successful access of Employment details link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d8e96287-fccf-45e1-bac5-2075e5a98d9f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Employment Details/Verify back button is clickable or not in Employment details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2d670455-b8e5-4b7f-a506-785f049f3f9b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Academic Info/Verify if able to access Academic Info link successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0364edba-af5c-4262-9403-29b020aaaf73</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Academic Info/Verify if back button is clickable or not in Academic Info</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>022cc133-68df-4fa9-a3ca-80554431041c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Work Experience/Verify successful access of Work Experince link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4fe1c048-feaa-458f-8536-9f95ab54c44d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Work Experience/Verify back button is clickable or not in Work Experience</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>409c1ade-775d-4a7d-b1fc-71d9334f468a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Family Details/Verify Successful access of family details link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b73427e3-7eed-4a1c-8636-74a6eb2c5f08</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Family Details/Verify back button is clickable or not in Family Details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a39c156e-9172-43ed-8f73-cb8653042e87</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Documents/Verify Successful access of Documents link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>89b5bc0b-7271-448c-8113-e14032e19679</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Documents/Verify back button is clickable or not in Documents</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8abf86b0-2a90-4f48-a611-d4bb21c8e1c9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Other details/Verify if scrollable to Other details link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>56285ecb-50dd-449d-bff8-fcdc4d25195c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Other details/Verify successful access of Other details link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>29bea4f1-927c-4eae-8f14-826f7fddbf2a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Other details/Verify back button is clickable in Other details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>645a718d-b458-492b-a33a-beca65d81b82</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Verify back button is clickable in Personal Info</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
