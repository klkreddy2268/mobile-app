import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

'Click on "Personal Info" tab to view logged in Staff details'
Mobile.tap(findTestObject('Personal Info'), 0)

Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)

'Click on "Personal Details" tab to view details'
Mobile.tap(findTestObject('Personal Details Menu Click'), 0)

Mobile.verifyElementText(findTestObject('Personal Details- Date of birth text'), 'Date of birth')

Mobile.verifyMatch('Married', 'Married', true)

'Minimize "Personal details" tab'
Mobile.tap(findTestObject('Admin/android.widget.TextView1 - Personal Details'), 0)

'Maximise "Contact Details" tab'
Mobile.tap(findTestObject('Admin/android.widget.TextView3 - Contact Details'), 0)

Mobile.verifyElementText(findTestObject('Contact Details- Mobile Number Text to be visible'), 'Mobile Number')

'Minimise "Contact Details" tab'
Mobile.tap(findTestObject('Admin/android.widget.TextView3 - Contact Details'), 0)

'Maximise "Present Address" details tab'
Mobile.tap(findTestObject('Admin/android.widget.TextView5 - Present Address'), 0)

Mobile.verifyElementText(findTestObject('Present Address- Address Text to be visible'), 'Address')

'Minimise "Present Address" details tab'
Mobile.tap(findTestObject('Admin/android.widget.TextView5 - Present Address'), 0)

'Maximise "Permanent Address" details tab'
Mobile.tap(findTestObject('Admin/android.widget.TextView7 - Permanent Address'), 0)

Mobile.verifyElementText(findTestObject('Permanent Address - Address text to be visible'), 'Address')

'Click on "back button" in Personal details'
Mobile.tap(findTestObject('Admin/android.widget.ImageButton0'), 0)

'Click on "Employemnt Details" tab'
Mobile.tap(findTestObject('Employment Details Tab'), 2)

Mobile.verifyElementText(findTestObject('Employment Details- Staff Type Text'), 'Staff Type')

Mobile.scrollToText('EPF', FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementText(findTestObject('Employment Details - EPF Text'), 'EPF')

'Click on "back button" in Employment details'
Mobile.tap(findTestObject('Admin/android.widget.ImageButton0'), 2)

'Click on "Academic Info" details tab'
Mobile.tap(findTestObject('Admin/Academic Info Tab'), 2)

Mobile.verifyElementText(findTestObject('Admin/Academic Info - Education Text'), 'Education')

'Click on back button in Academic Info details'
Mobile.tap(findTestObject('Admin/android.widget.ImageButton0'), 2)

'Click on "Work Experience" tab'
Mobile.tap(findTestObject('Work Experience tab'), 2)

'Click on "Back Button" in Work Experience details'
Mobile.tap(findTestObject('Admin/android.widget.ImageButton0'), 2)

'Click on "Family Details" tab'
Mobile.tap(findTestObject('Family Details tab'), 2)

'Click on "back button" in Family details'
Mobile.tap(findTestObject('Admin/android.widget.ImageButton0'), 2)

Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)

Mobile.scrollToText('Documents', FailureHandling.STOP_ON_FAILURE)

'Click on "Documents" Tab'
Mobile.tap(findTestObject('Documents Text View'), 5)

Mobile.waitForElementPresent(findTestObject('Documents Text View'), 5)

'Click on back button in Documents'
Mobile.tap(findTestObject('Documents back button'), 0)

Mobile.scrollToText('Other Details', FailureHandling.STOP_ON_FAILURE)

'Click on Other details Tab\r\n'
Mobile.tap(findTestObject('Other Details Tab'), 0)

'Click on back button in other details'
Mobile.tap(findTestObject('Other Details back button'), 0)

Mobile.tap(findTestObject('Personal Info Back button'), 0)

