import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

not_run: WebUI.waitForElementVisible(findTestObject('School Logo'), 10)

Mobile.tap(findTestObject('Settings/Settings Button'), 30)

Mobile.tap(findTestObject('Settings/Home Button'), 30)

Mobile.tap(findTestObject('Settings/Settings Button'), 30)

Mobile.delay(30, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Settings/Setting Button'), 5)

Mobile.tap(findTestObject('Settings/Push Notification Toggle Button'), 5)

Mobile.tap(findTestObject('Settings/Push Notification Yes'), 5)

Mobile.tap(findTestObject('Settings/Setting Back button in push notification'), 5)

Mobile.tap(findTestObject('Settings/Home Button'), 5)

Mobile.tap(findTestObject('Settings/Notification bell Icon'), 5)

Mobile.tap(findTestObject('Settings/Notifictions back button'), 5)

Mobile.tap(findTestObject('Settings/Setting Button'), 5)

Mobile.tap(findTestObject('Settings/Logout'), 5)

