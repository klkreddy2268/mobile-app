import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.verifyElementVisible(findTestObject('Parent Login/Main Menu after Login/Student Profile/Student Details/Student/Personal Details/Staff Name'), 
    0)

Mobile.verifyElementVisible(findTestObject('Parent Login/Main Menu after Login/Student Profile/Student Details/Student/Personal Details/Date of Birth'), 
    0)

Mobile.verifyElementVisible(findTestObject('Parent Login/Main Menu after Login/Student Profile/Student Details/Student/Personal Details/Nationality'), 
    0)

Mobile.verifyElementVisible(findTestObject('Parent Login/Main Menu after Login/Student Profile/Student Details/Student/Personal Details/Religion'), 
    0)

Mobile.verifyElementVisible(findTestObject('Parent Login/Main Menu after Login/Student Profile/Student Details/Student/Personal Details/Caste'), 
    0)

Mobile.verifyElementVisible(findTestObject('Parent Login/Main Menu after Login/Student Profile/Student Details/Student/Personal Details/Sub-Caste'), 
    0)

Mobile.verifyElementVisible(findTestObject('Parent Login/Main Menu after Login/Student Profile/Student Details/Student/Personal Details/Identification marks'), 
    0)

Mobile.verifyElementVisible(findTestObject('Parent Login/Main Menu after Login/Student Profile/Student Details/Student/Personal Details/Student Aadhar Number'), 
    0)

