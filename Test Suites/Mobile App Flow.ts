<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteCollectionEntity>
   <description></description>
   <name>Mobile App Flow</name>
   <tag></tag>
   <executionMode>SEQUENTIAL</executionMode>
   <testSuiteRunConfigurations>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Mobile</groupName>
            <profileName>default</profileName>
            <runConfigurationData>
               <entry>
                  <key>deviceName</key>
                  <value>Genymotion Motorola Moto X - 4.4.4 - API 19 - 720x1280 (Android 4.4.4)</value>
               </entry>
               <entry>
                  <key>deviceId</key>
                  <value>192.168.205.102:5555</value>
               </entry>
            </runConfigurationData>
            <runConfigurationId>Android</runConfigurationId>
         </configuration>
         <runEnabled>false</runEnabled>
         <testSuiteEntity>Test Suites/Mobile App Test Cases</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Mobile</groupName>
            <profileName>default</profileName>
            <runConfigurationData>
               <entry>
                  <key>kobitonDevice</key>
                  <value>{
  &quot;id&quot;: 82854,
  &quot;udid&quot;: &quot;ad051703c00eccc2e5&quot;,
  &quot;isBooked&quot;: false,
  &quot;capabilities&quot;: {
    &quot;udid&quot;: &quot;ad051703c00eccc2e5&quot;,
    &quot;modelName&quot;: &quot;SM-G935F&quot;,
    &quot;deviceName&quot;: &quot;Galaxy S7 Edge&quot;,
    &quot;isEmulator&quot;: false,
    &quot;resolution&quot;: {
      &quot;width&quot;: 1080,
      &quot;height&quot;: 1920
    },
    &quot;platformName&quot;: &quot;Android&quot;,
    &quot;platformVersion&quot;: &quot;7.0&quot;,
    &quot;installedBrowsers&quot;: [
      {
        &quot;name&quot;: &quot;chrome&quot;,
        &quot;version&quot;: &quot;68.0.3440.91&quot;
      }
    ]
  },
  &quot;orientation&quot;: &quot;PORTRAIT&quot;,
  &quot;captureScreenShots&quot;: true,
  &quot;isHidden&quot;: false,
  &quot;isOnline&quot;: true,
  &quot;isFavorite&quot;: true,
  &quot;isCloud&quot;: true
}</value>
               </entry>
            </runConfigurationData>
            <runConfigurationId>Kobiton Device</runConfigurationId>
         </configuration>
         <runEnabled>false</runEnabled>
         <testSuiteEntity>Test Suites/KobitonExecution</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Mobile</groupName>
            <profileName>default</profileName>
            <runConfigurationData>
               <entry>
                  <key>deviceName</key>
                  <value>samsung SM-G610F (Android 7.0)</value>
               </entry>
               <entry>
                  <key>deviceId</key>
                  <value>52009b6ab44b449d</value>
               </entry>
            </runConfigurationData>
            <runConfigurationId>Android</runConfigurationId>
         </configuration>
         <runEnabled>false</runEnabled>
         <testSuiteEntity>Test Suites/Mobile App Test Cases</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Mobile</groupName>
            <profileName>default</profileName>
            <runConfigurationData>
               <entry>
                  <key>deviceName</key>
                  <value>samsung GT-I9500 (Android 5.0.1)</value>
               </entry>
               <entry>
                  <key>deviceId</key>
                  <value>4d002db30eec21d9</value>
               </entry>
            </runConfigurationData>
            <runConfigurationId>Android</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/Mobile App Test Cases</testSuiteEntity>
      </TestSuiteRunConfiguration>
   </testSuiteRunConfigurations>
</TestSuiteCollectionEntity>
