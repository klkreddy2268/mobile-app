<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Login to test Scroll to Text for Transport Link</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>2d9a3e97-bcca-46bb-b45e-51934d2d1688</testSuiteGuid>
   <testCaseLink>
      <guid>7d29340e-6082-4bd7-bb12-7ecd193a760c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Login/Verify if able to successfully launch aplication</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9066c704-0cb8-41b2-9e3c-0c30205a3770</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Login/Verify if able to enter school code</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c06a787f-d13c-43dd-b531-8d66432c9023</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Login/Verify if able to click on proceed after entering school code</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bff62b17-8e19-48b0-966b-3a68959decb7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Login/Verify if able to click on Login with Google button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d2dfa5bf-e218-49ee-9510-6cf1f7834a97</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Login/Verify if able to select the available login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>009e3a35-dcc4-4a64-9407-33721e4a7d31</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Login/Verify if able to select Yes in Push Notifcations popup</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b0185c93-42d7-4e03-9eb6-c366031dd579</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Transport for Admin Staff/Verify successful access of Transport links</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3fe3c9be-3b30-4f95-bcc8-2c302c628bd7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Transport for Admin Staff/Verify expand button is clickable to view details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9834541e-9c9a-407b-90b0-8db6a3f3486c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Transport for Admin Staff/Verify able to click on Minimise button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>16befa10-17d4-457a-a38c-49820fad6d91</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Transport for Admin Staff/Verify able to click on Transport Call button to view details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b9535a2e-2726-4cc4-8ad2-8d6bce0bb769</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Transport for Admin Staff/Verify able to click on outside dialog to close the telephone details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b8152119-066a-4c27-98e9-dab2c35806f3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Transport for Admin Staff/Verify back button is clickable in transport</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
