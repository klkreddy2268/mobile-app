<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteCollectionEntity>
   <description></description>
   <name>NLP Mobile App Execution Summary</name>
   <tag></tag>
   <executionMode>SEQUENTIAL</executionMode>
   <testSuiteRunConfigurations>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Mobile</groupName>
            <profileName>default</profileName>
            <runConfigurationData>
               <entry>
                  <key>kobitonDevice</key>
                  <value>{
  &quot;id&quot;: 82788,
  &quot;udid&quot;: &quot;ZY2246BCKM&quot;,
  &quot;isBooked&quot;: false,
  &quot;capabilities&quot;: {
    &quot;udid&quot;: &quot;ZY2246BCKM&quot;,
    &quot;modelName&quot;: &quot;Moto E (4)&quot;,
    &quot;deviceName&quot;: &quot;Moto E (4)&quot;,
    &quot;isEmulator&quot;: false,
    &quot;resolution&quot;: {
      &quot;width&quot;: 720,
      &quot;height&quot;: 1280
    },
    &quot;platformName&quot;: &quot;Android&quot;,
    &quot;platformVersion&quot;: &quot;7.1.1&quot;,
    &quot;installedBrowsers&quot;: [
      {
        &quot;name&quot;: &quot;chrome&quot;,
        &quot;version&quot;: &quot;68.0.3440.85&quot;
      }
    ]
  },
  &quot;orientation&quot;: &quot;PORTRAIT&quot;,
  &quot;captureScreenShots&quot;: true,
  &quot;isHidden&quot;: false,
  &quot;isOnline&quot;: true,
  &quot;isFavorite&quot;: true,
  &quot;isCloud&quot;: true
}</value>
               </entry>
            </runConfigurationData>
            <runConfigurationId>Kobiton Device</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/NLP Prod Testcase Execution/Prod Execution - Copy</testSuiteEntity>
      </TestSuiteRunConfiguration>
   </testSuiteRunConfigurations>
</TestSuiteCollectionEntity>
