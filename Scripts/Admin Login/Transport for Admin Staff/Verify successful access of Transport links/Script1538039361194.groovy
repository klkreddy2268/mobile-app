import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import io.appium.java_client.MobileDriver
import io.appium.java_client.MobileElement as MobileElement
import io.appium.java_client.TouchAction
import io.appium.java_client.AppiumDriver as AppiumDriver
import com.detroitlabs.katalonmobileutil.touch.Scroll


def scrollToElementWithText(String elementsText, float toTopPosition = 0.8 * Mobile.getDeviceHeight()){		     def categoryText = findTestObject('Object Repository/Admin Login/Scroll to Transport Text in Main Menu to view details', [("Transport") : elementsText])  // some object in the repository with property text = ${text} condition = equals, detected by class+text		AppiumDriver<?> driver = MobileDriverFactory.getDriver()
		while (((Mobile.waitForElementPresent(categoryText, 2, FailureHandling.STOP_ON_FAILURE) == true) &&
		(Mobile.getElementTopPosition(categoryText, 2, FailureHandling.CONTINUE_ON_FAILURE) <= toTopPosition)) == false)           TouchAction ta = new TouchAction(driver)	   ta.press(5, 1430).waitAction().moveTo(5, 1200).release().perform()		}

//int timeout = 1
//Scroll.scrollListToElementWithText('Transport', timeout)
//Scroll.scrollListToElementWithText('Object Repository/Transport Menu', 'Transport', timeout)
//Mobile.tap(Finder.findLabel('Transport'), timeout)


//Mobile.waitForElementPresent(findTestObject('Object Repository/Attendance Tab'), 10)

//Mobile.scrollToText('Transport')

//Mobile.tap(findTestObject('Admin Login/Staff List/Tap on Transport to view transport deatils'), 0)


