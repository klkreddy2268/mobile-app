import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.remote.DesiredCapabilities as DesiredCapabilities
import com.kms.katalon.core.appium.driver.AppiumDriverManager as AppiumDriverManager
import com.kms.katalon.core.mobile.driver.MobileDriverType as MobileDriverType
import com.kms.katalon.core.util.internal.PathUtil as PathUtil
import io.appium.java_client.android.AndroidDriver as AndroidDriver

Mobile.startApplication('C:\\Users\\lakshmikanthk\\Downloads\\app-nlp-debug (2).apk', true)

Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)

//MobileBuiltInKeywords.verifyElementVisible(findTestObject('Settings/Update for App-Remind me later'), 0)
//Mobile.tap(findTestObject('Settings/Update for App-Remind me later'), 0)
MobileBuiltInKeywords.waitForElementPresent(findTestObject('Admin/android.widget.EditText0 (1)'), 0)

//Mobile.startApplication('kobiton-store:15288', true)
//String kobitonServerUrl = "https://klkreddy2268:1289f67a-8a3a-42bf-b843-9d8572a635e6@api.kobiton.com/wd/hub";
//DesiredCapabilities capabilities = new DesiredCapabilities();
//capabilities.setCapability("sessionName", "Automation test session");
//capabilities.setCapability("sessionDescription", "");
//capabilities.setCapability("deviceOrientation", "portrait");
//capabilities.setCapability("captureScreenshots", true);
//capabilities.setCapability("browserName", "chrome");
//capabilities.setCapability("deviceGroup", "KOBITON");
// For deviceName, platformVersion Kobiton supports wildcard
// character *, with 3 formats: *text, text* and *text*
// If there is no *, Kobiton will match the exact text provided
//capabilities.setCapability("deviceName", "PH-1");
//capabilities.setCapability("platformVersion", "7.1.1");
//capabilities.setCapability("platformName", "Android");
//AppiumDriverManager.createMobileDriver(MobileDriverType.ANDROID_DRIVER, capabilities, new URL('https://klkreddy2268:1289f67a-8a3a-42bf-b843-9d8572a635e6@api.kobiton.com/wd/hub'))
'Enter SchoolCode'
Mobile.setText(findTestObject('Admin/android.widget.EditText0 (1)'), 'DS', 0)

Mobile.waitForElementPresent(findTestObject('Admin/android.widget.Button0 - proceed (1)'), 10)

'Click on "Proceed" button\r\n'
Mobile.tap(findTestObject('Admin/android.widget.Button0 - proceed (1)'), 10)

'Click on "Login with Google" Button'
Mobile.tap(findTestObject('Admin/android.widget.FrameLayout7'), 0)

'Enter Gmail '
not_run: Mobile.setText(findTestObject('Google Sign in/Enter Email or phone'), 'klkreddy2268@gmail.com', 10)

'Click on Next'
not_run: Mobile.tap(findTestObject('Google Sign in/Click on Next Button'), 10)

'Enter Password for gmail account'
not_run: Mobile.setText(findTestObject('Google Sign in/Google Enter your password'), 'Next@123', 10)

'Click on Next'
not_run: MobileBuiltInKeywords.tap(findTestObject('Google Sign in/Click on Next after entering password'), 10)

'Accept Terms and Services'
not_run: MobileBuiltInKeywords.tap(findTestObject('Google Sign in/Google Terms of service I agree'), 10)

'Scroll to text for Accept button to be enabled'
not_run: Mobile.tap(findTestObject('Google Sign in/Google Services-Click on button to get Accept button'), 10)

'Click on Accept'
not_run: MobileBuiltInKeywords.tap(findTestObject('Google Sign in/Google Login- Accept'), 10)

'Choose the desired Gmail login which is registered with NLP'
Mobile.tap(findTestObject('Admin/android.widget.LinearLayout4'), 5)

'Choose "Yes" from the Push Notifications Popup'
Mobile.tap(findTestObject('Admin/android.widget.Button1 - Yes (1)'), 5)

Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)

not_run: RunConfiguration.getProjectDir() + (0 == 'Users\\lakshmikanthk\\Downloads\\app-nlp-debug.apk')

not_run: Mobile.startApplication(path, false)

not_run: Mobile.waitForElementPresent(findTestObject(null), 0)

