<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Mobile App Test Cases</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-09-18T15:03:02</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>6bf7f843-3fe9-4145-b801-6a19846b4ef1</testSuiteGuid>
   <testCaseLink>
      <guid>a42a4db1-0b3a-4395-b73d-0e6c7872ac78</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Verify able to login successfully with Google</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>656de7ac-a8b3-4c65-8d7e-6c754f3a88fa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Personal Details/Verify successful access of PersonalInfo links</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>40cef6b3-1c49-4b56-8725-cbfcb2222b9a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Attendance/Verify Successful access of Attendance links</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>75032fc4-7063-4ed9-a6c8-93e276ea808e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify successful access of Leave links</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a2a708cc-4fec-402b-8ad9-d658b782fd56</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>69fce78d-bddb-41de-b98a-ef35f3f6d740</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Transport/Verify successful access of Transport links</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>64937bc0-d58b-49cd-a89c-792fe98f2585</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Calendar/Verify successful access of Calendar links</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>26bae1ec-0151-4c5a-900a-864a1e85c141</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Messages/Verify successful access of Messages links</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
