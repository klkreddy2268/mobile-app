<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Working Copy-Staff Login Flow Execution</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>b1f2b83a-08ca-4393-80ac-4c522ecf1028</testSuiteGuid>
   <testCaseLink>
      <guid>fb73e372-56e6-4a28-b759-8c4a001f8074</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Login/Verify if able to successfully launch aplication</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4d948e05-117f-43bb-b2c2-0845371374fc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Staff Login/Login/Verify if able to click on Remind me later in the update popup</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4241a617-600b-4069-90d6-ddb5a029eb25</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Login/Verify if able to enter school code</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9c8f6965-5d72-430d-8f4e-81320b190536</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Login/Verify if able to click on proceed after entering school code</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>62800c4a-39df-419d-88b8-6a4b7f020e7c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Login/Verify if able to click on Login with Google button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>429b2679-07a5-4cd8-9192-ebbeee2abd17</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Login/Verify if able to select the available login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3602c9f1-57ff-4ec2-b52d-f834f7d8e98f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Login/Verify if able to select Yes in Push Notifcations popup</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>622df778-2166-4412-9fa4-47cb1a943f36</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Personal Details/Verify successful access of PersonalInfo links</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b100a68c-0e80-4076-9fba-a9a0e0d3a5b5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Personal Details/Verify Successful access of Details link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3d53351c-016c-43da-8a03-5e272b431c3e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Personal Details/Verify Successful access of Contact details link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a67fddb5-1d00-4dca-b7ce-e43fc7010da4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Personal Details/Verify Successful access of Present Address details link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>16127cb1-742b-45f6-b355-57d2aacf2109</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Personal Details/Verify Successful access of Permanent Address details link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4536e37b-8a1d-4a45-8fed-c06ab4b0d009</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Personal Details/Verify back button is clickable or not in Personal deatils</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7b609840-e142-4fc1-9a2f-214d5b54f2f6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Employment Details/Verify Successful access of Employment details link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3b1827b5-3de3-4646-a6c1-07730faa53d2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Employment Details/Verify back button is clickable or not in Employment details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2dc16933-42a4-4a16-ba09-ce1891fdb75e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Academic Info/Verify if able to access Academic Info link successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>26ee541f-0618-44a5-9feb-2e655c7e3d7a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Academic Info/Verify if back button is clickable or not in Academic Info</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4caab115-02a1-4247-9306-9e9f88d8e1ee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Work Experience/Verify successful access of Work Experince link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ee2a25a9-c274-46d0-8f14-e3df1ce11be8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Work Experience/Verify back button is clickable or not in Work Experience</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>677107ed-df7f-4d1e-999c-d4eea46c0d48</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Family Details/Verify Successful access of family details link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b1e3f4a4-a439-4b61-b19f-29fe0c9d34b5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Family Details/Verify back button is clickable or not in Family Details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0f219f80-b505-4b58-b8e5-e564f02b03c2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Documents/Verify Successful access of Documents link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c330b18a-2306-44fe-a59c-7e9b81b01a13</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Documents/Verify back button is clickable or not in Documents</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bb865e0a-9fe8-4689-a313-a98d9a40c41b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Other details/Verify if scrollable to Other details link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2a412b39-8724-4182-b71a-4528c6634759</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Other details/Verify successful access of Other details link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d00aae9a-63b7-4006-8cfe-c13885ce74f7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Other details/Verify back button is clickable in Other details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9639917c-0c1c-4a16-9ed8-d4e2417c1ba4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Verify back button is clickable in Personal Info</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>89d8efa1-0a66-497f-96f2-a4a4f84d22fc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Attendance/Verify Successful access of Attendance links</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a791b0fc-8e64-47a8-9c5d-c1c842f45a21</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Attendance/My Attendance/Verify Successful access of My Attendance</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6a327813-7b01-405b-98ab-ac3ae68ce2d4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Attendance/My Attendance/Verify sucessfull access to Attendance History in My Attendance page to August-2018</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>099ee120-3fe3-4f42-a4bd-45f3735ac17d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Messages/Verify back button is clickable in message</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e14a91ec-cbfc-42f6-93c8-9bfcc82509b0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Attendance/My Attendance/Verify back button is clickable in My Attendance</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>960fe0ee-5487-4120-b7e1-293f4def4e92</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Attendance/Roll Call/Verify Successful access of Roll Call link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6bbf7860-5e10-4815-9424-7883eef2c89c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Attendance/Roll Call/Verify Successful selection of first class secton from the populated list</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b116734e-1754-4b69-b27a-3fcba97a3ff3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Attendance/Roll Call/Verify Successful attendance marking for student</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0e1808b2-5888-42c8-84c4-de47e5fb7717</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Attendance/Roll Call/Verify back button is clickable in Roll Call</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7eaa1232-d682-457e-a8b2-1f1ee77fec67</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Attendance/Mark/Verify Successful access of Mark link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5836c07a-2d8f-4069-bca6-a8f1185686cc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Attendance/Mark/Verify Successful selection of Class Section</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>26bba309-b1eb-4a21-8c60-30b7f0a59db2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Attendance/Mark/Verify Successful student selection for marking Attendance</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c54762b1-c16f-46c5-9fb8-c08aac10ce99</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Attendance/Mark/Verify Successful marking of Attendance</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a7ae41eb-7e0a-4a8c-b218-ec86ba679a29</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Attendance/Mark/Verify Successful submit after selecting attendance status</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0afa5671-cd23-4ea6-8085-73196e4cb45f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Attendance/Mark/Verify back button is clickable in Mark Attenance Class Section student list</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>02131477-6e3f-44ca-900f-eacc19fcd2fc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Attendance/Mark/Verify back button is clickable in Class Section list page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2930815d-a23c-4f91-a414-6dea62381aea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Attendance/Mark/Verify back button is clickable in Attendance</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6f0964ec-7c8d-4686-811e-f3c17fd407f5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Calendar/Verify successful access of Calendar links</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3fed3cca-c77d-4b83-ab47-3b3d63f4c749</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Calendar/Verify Day View link is clickable</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>74b0902e-8854-4ada-8426-b17d22415242</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Calendar/Verify Week View link is clickable</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a314871c-6e8e-4918-8e93-772410cf6dd7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Calendar/Verify Month View is clickable</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>42362a23-5bce-4430-9482-f2e9bbc5dc5a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Calendar/Verify back button is clickable in Calendar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a2543986-3978-4526-9634-fa3f7752a4db</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify successful access of Leave links</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a2a708cc-4fec-402b-8ad9-d658b782fd56</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>74bc6f19-ef6e-4aab-8782-5599db2840d7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify Birthday Leave is clickable</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>98cf25b0-45d5-4cb5-b033-2bfbf7133a56</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify back button is clickable in Sick Leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1a61d0c3-fc3f-46af-8df5-789b0e3f5835</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify Casual Leave is clickable</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>74e2f49a-cd11-4263-8702-cf023edb1dfd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify back button is clickable in Casual Leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>05093c0f-7ea8-45ed-83e4-d990d1b658da</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify Compoff is clickable</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bd522665-7c9b-443e-8b69-afc8e221247d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify back button is clickable in Compoff</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>912be8b1-4832-4167-959a-4f52f31f2b96</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify Earned Leave is clickable</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d2bb2c99-0af8-4895-b1e9-786a1fb230e6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify back button is clickable in Earned Leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0e6238b0-dd7b-4cca-9268-da83cba06a35</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify Marraige Leave is clickable</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cca09774-51e1-43a5-9692-ace4262b9716</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify able to scroll to Sick Leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bd82ddae-bad4-4f39-83dc-ffe9296265dd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify Sick Leave is clickable</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5426b6e7-4c0f-4f41-92ba-88f854956a16</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify able to click on Start Date for applying leave or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3ec38e91-0deb-4dfd-a580-deb7700b82d1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify able to select Form Date and To Date for applying Leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2033433d-2e80-4b5e-912c-7195e1d94119</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify able to click on OK button for confirming Start Date and End Date for applying leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>58af6c8c-9a99-4c4e-8638-f79be5a787e0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify End date is clickable or not for selecting end date</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>58934805-57aa-4427-bcd1-014c5b4213d0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify if able to select End date from the date picker or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a7ddde54-67ab-49ab-8648-a6ecd7b3deed</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify able to click on OK button for confirming Start Date and End Date for applying leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>814554fd-9739-47ab-852c-8c5b87fab6d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify if able to enter reason for applying leave in text box</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bf7e42b1-4c6b-464e-8336-c843d6c18374</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify if able to click on Upload File button for uploading file</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0e29a121-65ca-45f2-afe9-048db57bf438</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify if able to select file for upload</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>74f9910a-5e16-43a8-be48-e61f11a73a87</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify able to click on Apply button for submitting sick leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2b3cb3d3-e3de-4e9a-8f9e-ae58c72586a3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Approvals/Verify able to click on Approvals tab</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>af878b06-cb03-4fad-a147-c7b8f5366252</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Approvals/Verify able to click on Student button for Approvals</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>13d7eb7d-377a-45df-97c8-bec68c3ec916</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Approvals/Verify able to click on Approve for approving student leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>57f6544f-052f-4d88-ae2e-8c1a827fa64b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Approvals/Verify able to enter remark for approval after selecting Approve</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>30ef0222-22c0-4466-b877-f8a524db1e75</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Approvals/Verify able to submit after enetering remark for student leave approval</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e1782a7d-6eb9-474d-a7e3-c95aea4bbdcd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Attendance/My Attendance/Verify back button is clickable in Attendance History Page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>24a22e4c-f1b5-46f7-bc0f-cde08fe4aeea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Verify able to click on History icon</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2ba8b53c-697c-4028-a1af-ab5b91417d39</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Leave-History-All/Verify able to click on All button in History</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d5ae8f3c-e42d-4f17-9dcb-5d25626ded75</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Leave-History-All/Verify able to click on leave to view details in all</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3201fc7a-fb2f-4b71-90df-d80de6277264</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Leave-History-All/Verify able to click on Withdraw button after selecting leave in all</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>13e02867-ad68-4a49-961a-4b3cc7d36468</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Leave-History-All/Verify able to enter remark for reject reason in all</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>96348fdc-53cf-4d10-8f4d-8098885c044d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Leave-History-All/Verify able to click on Done button after entering reason for rejection</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d9dbf952-9b63-4e8c-812b-39cdd8f34714</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Leave-History-Pending/Verify if able to click on Pending button in Leave History</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9f2ac0bb-4631-426a-9bee-de94f415cb9e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Leave-History-Pending/Verify if able to click on Sick Leave in pending</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7cc54dd4-659b-4869-b07d-d3e04ef91dd4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Leave-History-Pending/Verify if able to click on Withdraw button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1e4ac2ce-d82a-4880-b47a-67b6d02d04d3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Leave-History-Pending/Verify if able to enter remark for withdrawal in pending</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7abb2e7e-b71a-49ac-a401-edb84ac31484</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Leave-History-Pending/Verify if able to click on Done after entering reason for rejection in pending</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>05112126-f24c-4298-868e-330600c5a91b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Leave-History-Approved/Verify if able to click on Approved button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ae20d0db-1729-49e7-bc7a-832a37a9128b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Leave-History-Approved/Verify if able to click on Approved leave to view details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bfc1745f-501d-4362-a8b0-76d26d7efcb7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Leave-History-Approved/Verify back button is clickable in Approved leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cbc4e5e8-b6e0-4d68-bfc6-4bec9483edc6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Leave-History-Rejected/Verify if able to click on Rejected button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d856770e-9034-4dfa-b081-c820ac7df2b7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Leave-History-Rejected/Verify if able to click on leave to view details in rejected tab</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0c5321ce-070e-41c9-949a-d280dcf72d99</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Leave-History-Rejected/Verify back button is clickable in selected leave under rejected tab</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>279e7937-51ae-4f06-846a-9428849dca0c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Leave-History-Cancelled/Verify if able to click on Cancel button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>006b8658-7157-4d89-9cb2-b84409bb5d0d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Leave-History-Cancelled/Verify if able to click on cancelled leave to view details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7f5576a3-55a2-40f5-a702-d23994850ded</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Leave-History-Cancelled/Verify back button is clickable in cancelled leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>50884d83-4b05-4d31-ae47-4cd102758846</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Leave-History-Rejected/Verify if able to click on back button in leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>09af4d9a-63cf-4af4-bdf6-681dfb05637d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Verify back button is click in leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e6c52c1b-63c9-47fe-9414-064b374d9fcf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Transport/Verify successful access of Transport links</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>515083c6-5307-4dc3-8de3-349d90a7b304</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Transport/Verify expand button is clickable to view details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>da15843f-ded3-43d4-a019-d555f1e1fc3b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Transport/Verify able to click on Minimise button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c8084c1b-c374-4f22-89e4-89b96bf83002</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Transport/Verify able to click on Transport Call button to view details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e6cc207c-4fd2-43c9-9a26-b2dfce0be99d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Transport/Verify able to click on outside dialog to close the telephone details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d805cdca-219e-4e9e-bf7b-17dd5268ee1c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Transport/Verify back button is clickable in transport</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
