import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.eclipse.persistence.internal.oxm.record.json.JSONParser.value_return as value_return
import org.openqa.selenium.Keys as Keys

//import com.kms.katalon.core.testobject.ConditionType as ConditionType
'Click on "Personal Details" tab to view details'
Mobile.tap(findTestObject('Personal Details Menu Click'), 0)

'Date of Birth is validated'
not_run: def dateofbirth_actual = Mobile.getText(findTestObject('Object Repository/Personal Details Folder/Date of Birth of Staff- 01 Jan 1986'), 
    5)

not_run: def dateofbirth_expected = findTestData('Staff Details').getValue(2, 3)

not_run: Mobile.verifyMatch(dateofbirth_actual, dateofbirth_expected, false)

'Blood Group data is validated'
not_run: def bloodgroup_actual = Mobile.getText(findTestObject('Object Repository/Personal Details Folder/Staff Blood Group - A'), 
    5)

not_run: def bloodgroup_expected = findTestData('Staff Details').getValue(2, 4)

not_run: Mobile.verifyMatch(bloodgroup_actual, bloodgroup_expected, false)

'Minimize "Personal details" tab'
Mobile.tap(findTestObject('Admin/android.widget.TextView1 - Personal Details'), 0)

