<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Leave</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>70542d5c-f583-4ad7-8dd8-b14d67da48f0</testSuiteGuid>
   <testCaseLink>
      <guid>b89bdf06-fcb0-4413-9146-da5026e8dbf3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify successful access of Leave links</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a2a708cc-4fec-402b-8ad9-d658b782fd56</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>40b72ede-f557-445b-85bc-a80fb066fb48</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify Birthday Leave is clickable</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2d854a13-1c1c-44a2-bb4c-55932dae1436</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify back button is clickable in Sick Leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3b083a24-fdfe-4433-8354-7dfa4dadbb34</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify Casual Leave is clickable</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>11ec6f78-3589-4c49-97bc-1857e943e4ea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify back button is clickable in Casual Leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ad412963-8e6c-4f7c-a577-9640531ac511</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify Compoff is clickable</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>26933a50-75ee-4b38-9e7d-ed5d4d06f6f5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify back button is clickable in Compoff</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8a9b67ab-dcab-4df2-9171-af4f572f48fe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify Earned Leave is clickable</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>25430fd7-c73e-4eb9-a79a-ebe1f3f2f4d8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify back button is clickable in Earned Leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>814f81ac-fb23-4454-8384-a2070e6921bb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify able to scroll to Sick Leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>607f71ab-d990-49a6-8ef7-f89ec133b0bb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify Sick Leave is clickable</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7a9c0887-b3c0-43a9-9f8c-4c9045abccb3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify able to click on Start Date for applying leave or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>79c696c2-41e8-4d0a-b8d6-9c5d9b5421a0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify able to select Form Date and To Date for applying Leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>45e7244b-e05f-4438-8979-a9151814c863</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify able to click on OK button for confirming Start Date and End Date for applying leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>21265b62-ddf2-4f3d-ba17-37e724a6e8ae</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify able to click on 2nd Half radio button for applying leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1902cfa4-703f-4b7b-8d79-4dea3c691e5a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify if able to enter reason for applying leave in text box</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3bd35313-3868-4aa0-a0a2-4dc83147afc9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify if able to click on Upload File button for uploading file</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b720ee4c-a10f-492d-b436-17ae37c75002</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify if able to select file for upload</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2dd90eae-757a-42e0-8d2e-fc58693d914e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Apply/Verify able to click on Apply button for submitting sick leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6eaad6fb-2d79-46c2-a7ad-b92723e1031a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Approvals/Verify able to click on Approvals tab</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e62ff673-a265-49ec-884f-6086a22ead41</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Approvals/Verify able to click on Student button for Approvals</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>410f2faf-6f45-432c-aa2f-7ddf0b172103</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Approvals/Verify able to click on Approve for approving student leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>29fdd64e-e552-4442-bc03-79b2f27d5e7b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Approvals/Verify able to enter remark for approval after selecting Approve</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>27710dcd-ab71-4cd8-9f10-6e44b0281c53</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Approvals/Verify able to submit after enetering remark for student leave approval</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d6f8cb35-98f6-4e11-a111-6b47bb9ac69d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-Approvals/Verify back button is clickable in Leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fc5c90ed-4316-4853-a4cf-702000f808d4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Verify able to click on History icon</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1df0b2ee-9ee1-453d-84b4-e9515b31bfcd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Leave-History-All/Verify able to click on All button in History</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7e979055-b58a-48b8-92d0-3c6a7b43d808</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Leave-History-All/Verify able to click on leave to view details in all</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9599cc63-f648-4861-9cdc-35007bf3724f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Leave-History-All/Verify able to click on Withdraw button after selecting leave in all</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e79d9cb2-177e-4549-a79b-749026cc7936</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Leave-History-All/Verify able to enter remark for reject reason in all</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>03bf3aaa-6e1f-4e3d-b01c-e57d9656e134</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Leave-History-All/Verify able to click on Done button after entering reason for rejection</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>20024122-064c-47f7-a0e9-11354223c916</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Leave-History-Pending/Verify if able to click on Pending button in Leave History</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0d3222dc-b0d7-4728-b5cd-0a1100d841ed</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Leave-History-Pending/Verify if able to click on Sick Leave in pending</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>83998098-b498-410a-adb7-f26aac696ed9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Leave-History-Pending/Verify if able to click on Withdraw button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>21360986-86cd-4214-bdf6-d7b70ec3e7e7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Leave-History-Pending/Verify if able to enter remark for withdrawal in pending</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>56d520c0-4db0-4a77-85f2-5e12b2e4164a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Leave-History-Pending/Verify if able to click on Done after entering reason for rejection in pending</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f8be2702-45ca-4cbd-8271-fe215fd33e0c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Leave-History-Approved/Verify if able to click on Approved button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>35022419-5c1c-470f-b3b5-1202b8b19cca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Leave-History-Approved/Verify if able to click on Approved leave to view details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8681b2d4-4ebc-4163-8509-a73bc816417a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Leave-History-Approved/Verify back button is clickable in Approved leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>977c58dd-b64e-4f3d-b0de-e6d010111e98</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Leave-History-Rejected/Verify if able to click on Rejected button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dba22c76-65c2-448f-a5cc-df2324e5f491</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Leave-History-Rejected/Verify if able to click on leave to view details in rejected tab</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2141b53e-5373-4bde-b454-6ed281a6303a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Leave-History-Rejected/Verify back button is clickable in selected leave under rejected tab</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>08065dfc-f376-482d-b2b1-ad7e1f00362a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Leave-History-Cancelled/Verify if able to click on Cancel button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>970e3df7-fe7b-4c56-845d-49b0df16ef94</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Leave-History-Cancelled/Verify if able to click on cancelled leave to view details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>11cb5640-b993-456e-9985-0239b76ac9e1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Leave-History-Cancelled/Verify back button is clickable in cancelled leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6cb1c615-1b72-45e6-abad-bbcb7a155304</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Leave-History-Rejected/Verify if able to click on back button in leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>551f1aec-c1cd-4ecf-8811-3466072ab63b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Leave/Leave-History/Verify back button is click in leave</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
