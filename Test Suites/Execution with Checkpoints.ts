<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Execution with Checkpoints</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-09-04T11:14:17</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>c27f1004-fb5f-4cf6-8718-1dad62ddd730</testSuiteGuid>
   <testCaseLink>
      <guid>00b982f6-4f0e-42e2-a14e-55b8f21d35f0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Verify able to login successfully with Google</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e73e9c69-3dc2-40a8-85be-a3d03defd91c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Verification with Checkpoints/Personal Info with check points</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
