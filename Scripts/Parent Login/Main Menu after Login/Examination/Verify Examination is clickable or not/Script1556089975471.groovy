import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

not_run: Mobile.waitForElementPresent(findTestObject('Parent Login/Main Menu after Login/Examination/Examination Tab (Scroll)'), 
    45)

not_run: Mobile.scrollToText('Examination')

not_run: def examinationText = Mobile.getText(findTestObject('Parent Login/Main Menu after Login/Examination/Scroll to Examination'), 
    5)

not_run: Mobile.verifyEqual(examinationText, 'Examination')

Mobile.tap(findTestObject('Parent Login/Main Menu after Login/Examination/Examination Tab (Scroll)'), 3)

