<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Transport</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>ca57bb09-4867-44fb-b223-8d59f6858a5e</testSuiteGuid>
   <testCaseLink>
      <guid>d5e19ca8-6af5-4a10-a300-106856d721aa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Transport/Verify successful access of Transport links</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>61568917-6c7c-407b-a846-443d39e70a69</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Transport/Verify expand button is clickable to view details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d177fd01-852f-4802-b93e-7338293574a6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Transport/Verify able to click on Minimise button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ad530dad-0f87-4aab-8e43-1dc022419574</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Transport/Verify able to click on Transport Call button to view details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c265b56f-8c48-49dd-ae6a-e103040ff449</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Transport/Verify able to click on outside dialog to close the telephone details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>62d3ea50-8c0d-4e3b-a161-60f4a13abb48</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Transport/Verify back button is clickable in transport</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
