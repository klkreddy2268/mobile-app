<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Calendar</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>1e3407e7-7465-4c31-bb7f-aec48f78a0e5</testSuiteGuid>
   <testCaseLink>
      <guid>5353a865-24f8-477f-969e-47ccc6ec92c9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Calendar/Verify successful access of Calendar links</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>784992ec-a130-4443-a355-d95c9de5361a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Calendar/Verify Day View link is clickable</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e5155b58-7b4c-4318-b94a-096eb8b6cfc1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Calendar/Verify Week View link is clickable</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cf7756c7-a5da-465c-9291-d116e97640c3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Calendar/Verify Month View is clickable</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5dca07c3-258d-438b-88db-66ff8f146547</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Calendar/Verify back button is clickable in Calendar</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
