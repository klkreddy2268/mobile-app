<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Messages</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>f90f3da8-dc79-48a5-9dfd-1f46028c2bfd</testSuiteGuid>
   <testCaseLink>
      <guid>55f3228d-e3e7-47e8-988d-7c33bcc7c3c5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Messages/Verify successful access of Messages links</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dfd667ab-bae9-477f-9c26-7339afd76206</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Messages/SMS/Verify able to click on SMS Tab</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ad92cff7-03f6-4811-92df-564ca510edda</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Messages/EMAIL/Verify able to click on Email tab</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>84a18643-0945-4b01-a9ce-a26568b56eeb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Messages/NOTIFICATIONS/Verify able to click on Notifications tab</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c84198e0-a70d-42a9-8ab9-21b63763d1cc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Messages/Verify back button is clickable in message</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
