import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import org.openqa.selenium.Keys as Keys

'Verify "Leave" Menu is clickable or not'
Mobile.tap(findTestObject('Leave Menu'), 0)

'Check if able to click on back button Leave without pay or not'
not_run: Mobile.tap(findTestObject('Leave/Leave Types/Leave Without Pay Tab'), 5)

'Check if able to click on back button or not'
not_run: Mobile.tap(findTestObject('Leave/Leave Types/android.widget.ImageButton0'), 5, FailureHandling.CONTINUE_ON_FAILURE)

'Check if able to click on back button or not'
not_run: Mobile.tap(findTestObject('Leave/Leave Types/android.widget.ImageButton0'), 5, FailureHandling.CONTINUE_ON_FAILURE)

not_run: Mobile.delay(10)

not_run: Mobile.scrollToText('Maternity Leave')

not_run: def itemText = Mobile.getText(findTestObject('Leave/Leave Types/Maternity Leave Tab'), 5)

not_run: Mobile.verifyEqual(itemText, 'Maternity Leave')

'Check if able to click on Maternity leave or not'
not_run: Mobile.tap(findTestObject('Leave/Leave Types/Maternity Leave Tab'), 5)

'Check if able to click on back button or not'
not_run: Mobile.tap(findTestObject('Leave/Leave Types/android.widget.ImageButton0'), 5, FailureHandling.CONTINUE_ON_FAILURE)

not_run: Mobile.scrollToText('On Duty', FailureHandling.STOP_ON_FAILURE)

'Check if able to click on Onduty Leave or not'
not_run: Mobile.tap(findTestObject('Leave/Leave Types/On Duty Tab'), 5)

'Check if able to click on back button or not'
not_run: Mobile.tap(findTestObject('Leave/Leave Types/android.widget.ImageButton0'), 5, FailureHandling.CONTINUE_ON_FAILURE)

not_run: Mobile.scrollToText('Sabbatical Leave', FailureHandling.STOP_ON_FAILURE)

'Check if able to click on Sabbatical leave or not'
not_run: Mobile.tap(findTestObject('Leave/Leave Types/Sabbatical Leave'), 5)

'Check if able to click on back button or not'
not_run: Mobile.tap(findTestObject('Leave/Leave Types/android.widget.ImageButton0'), 5, FailureHandling.CONTINUE_ON_FAILURE)

not_run: Mobile.tap(findTestObject('Leave/Leave Types/android.widget.LinearLayout19 (1)'), 5)

not_run: MobileBuiltInKeywords.tap(findTestObject('Leave/Leave-Upload File - Click on Images'), 5)

not_run: MobileBuiltInKeywords.tap(findTestObject('Leave/Leave-File Upload-Image-Screenshot folder-image selection'), 0)

not_run: Mobile.verifyElementText(findTestObject('Leave/Leave-Upload File- Name for verification'), 'File size')

'Check if able to scroll to "Sick Leave" Text or not'
not_run: MobileBuiltInKeywords.scrollToText('4/4/2018 (SecondHalf)', FailureHandling.CONTINUE_ON_FAILURE)

not_run: Mobile.verifyElementText(findTestObject('Leave/History/History-All-Scroll to last leave type and Withdraw the pending leave'), 
    'Withdraw')

