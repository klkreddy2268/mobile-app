<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Attendance</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>1ecaa82c-88a5-4a25-9785-f01aac11c6b2</testSuiteGuid>
   <testCaseLink>
      <guid>feae7889-f489-4016-bb2c-c22d94ca4b87</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Attendance/Verify Successful access of Attendance links</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>23172548-59a4-4247-8ff1-e272cdf23730</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Attendance/My Attendance/Verify Successful access of My Attendance</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1be1fff6-ad44-444b-ab51-3d7587cb8477</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Attendance/My Attendance/Verify sucessfull access to Attendance History in My Attendance page to August-2018</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d0b66f9b-bf3f-43a8-a64f-95207c6d81b8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Attendance/My Attendance/Verify back button is clickable in My Attendance</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>43837c7f-9f8f-4da2-af1e-68e392f99e22</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Attendance/Roll Call/Verify Successful access of Roll Call link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5fd33ab3-c396-4f43-8f77-83b98c13f946</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Attendance/Roll Call/Verify Successful selection of first class secton from the populated list</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>216b2daa-5225-4849-85aa-ec8d68cbcd61</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Attendance/Roll Call/Verify Successful attendance marking for student</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f10cf9fb-da06-4c2d-a4d3-9666e3003ca3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Attendance/Roll Call/Verify back button is clickable in Roll Call</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c0924b4f-e3fe-4f89-9ec1-015725f45470</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Attendance/Mark/Verify Successful access of Mark link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>987522ad-539f-4555-8d21-a6ef2365fdf3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Attendance/Mark/Verify Successful selection of Class Section</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cb05bbd6-b01f-41ec-89b2-2e0efc129efd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Attendance/Mark/Verify Successful student selection for marking Attendance</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b737f9dd-0efb-4307-8ebe-c72821b91a60</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Attendance/Mark/Verify Successful marking of Attendance</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7f06053f-1000-4125-a679-db1bdf4004b6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Attendance/Mark/Verify Successful submit after selecting attendance status</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e1161188-4e18-491f-bdef-4b84721ecd3c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Attendance/Mark/Verify back button is clickable in Mark Attenance Class Section student list</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>09351f65-5fcc-429d-9a1e-d34095a676af</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Attendance/Mark/Verify back button is clickable in Class Section list page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b322e5aa-b7c3-444d-8541-3a69971752c1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Attendance/Mark/Verify back button is clickable in Attendance</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
