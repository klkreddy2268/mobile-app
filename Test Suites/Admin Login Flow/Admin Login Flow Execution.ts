<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Admin Login Flow Execution</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>1ec05930-47fa-4dd8-871c-90e7d4d192f5</testSuiteGuid>
   <testCaseLink>
      <guid>fb73e372-56e6-4a28-b759-8c4a001f8074</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Login/Verify if able to successfully launch aplication</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4241a617-600b-4069-90d6-ddb5a029eb25</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Login/Verify if able to enter school code</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9c8f6965-5d72-430d-8f4e-81320b190536</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Login/Verify if able to click on proceed after entering school code</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>62800c4a-39df-419d-88b8-6a4b7f020e7c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Login/Verify if able to click on Login with Google button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>429b2679-07a5-4cd8-9192-ebbeee2abd17</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Login/Verify if able to select the available login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3602c9f1-57ff-4ec2-b52d-f834f7d8e98f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff Login/Login/Verify if able to select Yes in Push Notifcations popup</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>622df778-2166-4412-9fa4-47cb1a943f36</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Personal Details/Verify successful access of PersonalInfo links</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b100a68c-0e80-4076-9fba-a9a0e0d3a5b5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Personal Details/Verify Successful access of Details link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3d53351c-016c-43da-8a03-5e272b431c3e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Personal Details/Verify Successful access of Contact details link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a67fddb5-1d00-4dca-b7ce-e43fc7010da4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Personal Details/Verify Successful access of Present Address details link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>16127cb1-742b-45f6-b355-57d2aacf2109</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Personal Details/Verify Successful access of Permanent Address details link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4536e37b-8a1d-4a45-8fed-c06ab4b0d009</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Personal Details/Verify back button is clickable or not in Personal deatils</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7b609840-e142-4fc1-9a2f-214d5b54f2f6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Employment Details/Verify Successful access of Employment details link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3b1827b5-3de3-4646-a6c1-07730faa53d2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Employment Details/Verify back button is clickable or not in Employment details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2dc16933-42a4-4a16-ba09-ce1891fdb75e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Academic Info/Verify if able to access Academic Info link successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>26ee541f-0618-44a5-9feb-2e655c7e3d7a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Academic Info/Verify if back button is clickable or not in Academic Info</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4caab115-02a1-4247-9306-9e9f88d8e1ee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Work Experience/Verify successful access of Work Experince link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ee2a25a9-c274-46d0-8f14-e3df1ce11be8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Work Experience/Verify back button is clickable or not in Work Experience</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>677107ed-df7f-4d1e-999c-d4eea46c0d48</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Family Details/Verify Successful access of family details link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b1e3f4a4-a439-4b61-b19f-29fe0c9d34b5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Family Details/Verify back button is clickable or not in Family Details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0f219f80-b505-4b58-b8e5-e564f02b03c2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Documents/Verify Successful access of Documents link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c330b18a-2306-44fe-a59c-7e9b81b01a13</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Documents/Verify back button is clickable or not in Documents</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bb865e0a-9fe8-4689-a313-a98d9a40c41b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Other details/Verify if scrollable to Other details link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2a412b39-8724-4182-b71a-4528c6634759</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Other details/Verify successful access of Other details link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d00aae9a-63b7-4006-8cfe-c13885ce74f7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Other details/Verify back button is clickable in Other details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9639917c-0c1c-4a16-9ed8-d4e2417c1ba4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Staff Login/Personal Info/Verify back button is clickable in Personal Info</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f5f89f05-b404-403d-bf4c-1e9cb4e2249d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Staff List/Verify if able to click on Staff List</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b5084022-b0ad-41d6-ae57-bd19ff416253</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Staff List/Verify if able to Click on Staff to view details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dc42d1a7-cc8c-4271-b949-ecf3665f5f42</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Staff List/Verify if able to click on mobile no icon</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c92b6dd3-d273-434b-a840-6fb87498cc56</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Staff List/Verify if able to press back button to navigate to app</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>83255398-67c4-48c6-a8e4-43b8c7795c25</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Staff List/Verify if able to click on Message icon</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>29640ab1-4d16-4fd7-83a2-52fc0f156585</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Staff List/Verify if able to click on backbutton to navigate back to app from Messages mode</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8d91fe75-a0d7-40c3-b822-806488e7c3b0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Staff List/Verify if able to click on Sell All</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>71559069-d96d-4a96-b6c8-bb97b04da62f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Staff List/Verify if able to click on Details Menu</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>06c9d3e8-1686-4998-a274-0c24d8b66e31</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Personal Info/Personal Details/Verify successful access of PersonalInfo links</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1a4aa164-2336-4f24-b193-8cba1b040147</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Personal Info/Personal Details/Verify Successful access of Details link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aa5699d0-b8bd-46a0-a330-bb2061ecb65f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Personal Info/Personal Details/Verify Successful access of Contact details link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>31adb6fc-de27-464d-b3e7-05125d7ef502</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Personal Info/Personal Details/Verify Successful access of Present Address details link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c86a6c07-8658-4be6-b2a3-793aa8461daf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Personal Info/Personal Details/Verify Successful access of Permanent Address details link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9743c4c5-042b-426f-986a-0c6d64f7c87c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Personal Info/Personal Details/Verify back button is clickable or not in Personal deatils</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>861bb9c7-589b-433e-9f93-5170fd651241</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Personal Info/Employment Details/Verify Successful access of Employment details link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fd4bbc01-8d66-4c19-ae1a-51b0ec7f91bb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Personal Info/Employment Details/Verify back button is clickable or not in Employment details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d47327c7-9903-4cb9-a9df-22d2ce314a30</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Personal Info/Academic Info/Verify if able to access Academic Info link successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e4ee2a2c-1e8f-47ff-9586-6b6737e92be0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Personal Info/Academic Info/Verify if back button is clickable or not in Academic Info</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>75f252ac-f44d-4a41-9dd4-45850a21502d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Personal Info/Work Experience/Verify successful access of Work Experince link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c2f98f22-7f19-4bdd-bd15-19b9923171be</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Personal Info/Work Experience/Verify back button is clickable or not in Work Experience</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5f62089a-424d-4162-8713-3559d885ff50</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Personal Info/Family Details/Verify Successful access of family details link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>23865ca2-3b0d-4398-8ecd-5b43e0c6ac4c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Personal Info/Family Details/Verify back button is clickable or not in Family Details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f83094f8-6898-4e55-9464-f417afd58cc5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Personal Info/Other details/Verify if scrollable to Other details link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c74b2991-c2ae-4412-b67d-eb12eb14413e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Personal Info/Other details/Verify successful access of Other details link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>765fd669-93c6-44ed-80ee-5efe34b66137</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Personal Info/Other details/Verify back button is clickable in Other details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>34e554ae-4400-4e23-a62c-58a18510d7ca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Staff List/Verify if able to click on back button to navigate view staff details menu</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>426ec425-aee8-41df-8e92-f3b67c8b217e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/HRMS/Verify if able to tap on HRMS link to view details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aa0b044f-b909-458b-a652-1a13ae006471</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/HRMS/Attendance/My Attendance/Verify Successful access of Attendance links</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>798a6498-dad6-422c-bcdd-7c06368df957</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/HRMS/Attendance/My Attendance/Verify if able to check status when tapped in selected date</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d2f5e873-ac4c-4611-b8d9-dffe610de220</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/HRMS/Attendance/My Attendance/Verify if able to scroll to Yearly Graph Details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7b90a9f9-7add-496d-8f71-29404bad2e48</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/HRMS/Attendance/My Attendance/Verify if able to click on Yearly Graph Details link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e6dae0be-dd62-4a86-8577-44174b3eb3fd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/HRMS/Attendance/My Attendance/Verify if able to click on back button in Yearly Graph Details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9fdf333e-e9c2-47a4-b396-647a8dcb4080</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/HRMS/Attendance/My Attendance/Verify if able to click on back button to view Attendance previous month</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5754cfa2-55a9-4b2f-821f-c1a71e2fbd32</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/HRMS/Attendance/My Attendance/Verify if able to tap on Next button to view current month attendance</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d3acf6aa-77a5-4dcc-9f0f-2ea188251887</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/HRMS/Attendance/My Attendance/Verify back button is clickable in Attendance</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>728ce95f-8ea6-43d9-8fbe-edf77f15423f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/HRMS/Leave/Verify if able to tap on Leave menu to view details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eb1b3197-3f71-4650-8125-417dacabf578</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/HRMS/Leave/Verify if able to click on Leave Type to minimise deatils</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fa00c89b-cf85-4a29-b0a1-bc5a9772fafd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/HRMS/Leave/Verify if able to tap on Leave History to view details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ed88f363-3815-43f9-b845-354afc557d91</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/HRMS/Leave/Verify if able to click on backbutton in leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d9fb5583-a76a-49be-b375-9d25703e91fe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/HRMS/Verify if able to tap on HRMS back button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eebeafa2-a554-465d-94c5-ff24894ec4e7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Transport/Verify successful access of Transport links</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8c18a96e-a650-4517-937c-fabc3eeee4c0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Transport/Verify expand button is clickable to view details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b171281f-0961-4493-b572-c877ea606d37</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Transport/Verify able to click on Minimise button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>50456e8e-b0b0-4375-adb5-31b0b05ece70</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Transport/Verify able to click on Transport Call button to view details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aa09b4f2-4bc9-423b-a1bd-6d81dd1e9c73</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Transport/Verify able to click on outside dialog to close the telephone details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3b79bf3b-0272-4c32-81c3-19aced0370b2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Transport/Verify back button is clickable in transport</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8054bcfb-64ab-43d3-92a0-fa4c2ff29941</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Personal Info/Documents/Verify Successful access of Documents link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c6cb5be5-1cbd-4987-b471-0349208a7aa1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Personal Info/Documents/Verify back button is clickable or not in Documents</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>49321d14-7ccf-47fd-bdd2-c2a612e7be11</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Personal Info/Verify back button is clickable in Personal Info</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0960f9d8-a265-4509-a226-2af9bcead554</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Staff List/Verify if able to scroll to Staff William Hill</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b3620c53-a60b-480c-81c3-33bbf81ccc70</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Staff List/Verify if able to tap on staff Willam Hill</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f5634474-c2b2-4f45-bb2d-462dafa9f472</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Staff List/Verify if able to click on back button in staff details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ad4f93ca-290f-4194-9985-fbfb5417830e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Staff List/Verify if able to tap on sell all to view willaim hall details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7de70c36-6ca1-4870-9d72-da05d1104257</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Staff List/Verify if able to tap on Name Filter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a0415ea7-0063-4158-8f8d-a8c6c12a00d4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Staff List/Verify if able to change sorting by department</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>713e5be1-ca71-4a1d-b798-d951600bbbc3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Staff List/Verify if able to click on Sort by filter for reseting</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>84c4964d-dacd-46ea-a498-ae3b960d77ce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Staff List/Verify if able to reset sort by filter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9e58d833-4d42-4e66-8199-4df0916bebf4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Staff List/Verify if able to click on filter by gender and department</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fd24d3e6-96ee-4433-b945-234887ba93d0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Staff List/Verify if able to select gender in filter list</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6520579f-9ade-4e3d-9311-1273fac4b87d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Staff List/Verify if able to select department in filter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>65946505-5784-4d63-9f32-ae6e277e1245</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Staff List/Verify if able to click on apply after selecting Gender and Department</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c6821ec0-de3e-465e-90cd-5b29389c8e5d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Staff List/Verify if able to reset filter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6602adc8-2e72-4dfe-8587-1e54b106a3db</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Staff List/Verify if able to click on Reset to reset filter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cc3ca62d-3b9a-45c9-b362-1e73c4cd4c12</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Staff List/Verify if able click on back button in filter page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aef44bb4-96c1-49b4-8a69-2feaf589d968</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Staff List/Verify if able to click on apply for changes in filter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>558267ee-6bf7-4f1c-9018-15c860444e07</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Staff List/Verify if able to click on search in Staff list</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>95e8174c-ae3e-4f66-9c07-d546883d2d10</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Staff List/Verify if able to type staff name to view staff in list</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7749ac52-649a-420f-84ba-50035af76b16</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Staff List/Verify if able to view the searched staff details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cb218b18-feae-4456-99e8-30e46803191b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Staff List/Verify if able to click on See All in Employee to view deatils</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e4fa79f6-8f38-407b-b946-b04445829a90</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Staff List/Verify if able to click on back button in employee details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>796363a7-4df3-4de7-9858-dff294326a84</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Staff List/Verify if able to click on back button in staff search to view staff details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>31384894-0820-4495-9b29-242831dc09d8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Staff List/Verify if able to click on back button in Staff details to main menu</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>196e3d0d-b44e-4231-a8b9-c5fee88e2335</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Verify if able to click on student List</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b19d5ae7-431d-4ed6-a31a-23ede4c97850</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Search/Verify if able to click on search in student details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fbc07aa5-d7ed-4934-af9c-248d2a24f7c7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Search/Verify if able to enter 1260 admission no to search</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>db1a1412-2305-42e8-8622-720c58cc84ce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Search/Verify if able to select 1260 admission from the suggested list</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>106d44aa-0a26-4608-ae9e-50eea088ae42</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Search/Verify if able to click on See all to view 1260 admission no details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5b7b15e2-1a6d-432d-a714-b7306aaa9464</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Verify if able to click on student to view details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bf1e882b-f7bc-41ab-aaac-d94f68f35763</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Verify if able to click on See All in student to view student details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6bb5dee3-f60f-41f9-864b-eddd0781585e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Verify if able to click on view details to view student details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>af812c8a-94bb-4fb7-bed8-18edf1fedbf3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Verify if able to click on Student to view details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3b57e876-f646-473b-8406-79066bef2807</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Verify if able to click on personal details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a67c4299-a7da-4a13-9554-80b316031bde</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Verify if able to click on Academic details to maximise</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1593e32d-ea81-4a08-8273-5a2b6e55e408</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Verify if able to tap on Academic details to minimise</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>164b500a-3ad8-43e3-ae2f-2676ed7ad8fa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Verify if able to tap on Contact Details to maximise</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>07935e09-b932-46c2-be53-2abe1ab7a230</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Verify if able to click on Contact details to Minimise</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a22ea9c1-f3eb-4d05-9a35-89f10cf47cc2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Verify if able to click on Previous School Details to maximise</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ae48a787-8707-475c-a4d2-ace8d31bff81</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Verify if able to click on Previous School Details to minimise</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dd009f2a-647b-4da3-a659-8a9833984938</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Tap on Miscellaneous details to maximise</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f7c4be7f-89e6-42af-8692-8c195eb56de1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Verify if able to click on Miscelleaneous details to minimise</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>58146512-c170-4380-8486-393063fc7f2a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Verify if able to click on back button in student details to main menu</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>49d99d9c-ffa9-4fb5-a3af-5c8f5751a1d4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Parents/Verify if able to click on Parents link to view details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8ad25a02-f07e-4236-84f8-bc0dcf9d62f3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Parents/Verify if able to click on back button in parent details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c8d3b365-297f-41ce-9977-4211619a98e1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Gaurdians/Verify if able to click on Gaurdians link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b0142121-0af1-4881-9519-87cc479fa5eb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Gaurdians/Verify if able to click on back button in Guardian Details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>88a79e77-8626-492f-b1bb-ac15e3799ee9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Verify if able to click on back button in student details for accessing other links</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>747cc2c1-ad45-4b67-b8fc-d384aae7d323</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Attendance and Discipline/Verify if able to click on Attendance and Discipline link to view details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>21708a58-cbd4-40cb-910f-c0b84e979bc9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Attendance and Discipline/Verify if able to click on Attendance status to view details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>26877fc4-1944-4271-b193-4776370dec42</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Attendance and Discipline/Attendance Status/Verify if able to click on current month to view Attendance</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>60890c15-ada2-4f1a-aefe-1c365b645c5f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Attendance and Discipline/Attendance Status/Verify if able to click on date to view status</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3e778afb-8c0a-4374-9204-3612b2e4203b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Attendance and Discipline/Attendance Status/Verify if able to click on Future month</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>08506afe-49a9-4746-8b8a-27fc88615b30</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Attendance and Discipline/Attendance Status/Verify if able to click on back button in Attendance to view Months View</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4437d664-7ea0-4dfc-a6c2-fb5e70a807ef</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Attendance and Discipline/Attendance Status/Verify if able to click on Attendance back button to access other menus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cfa5098d-0e51-4c75-852a-29271e21fb29</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Attendance and Discipline/Discipline/Verify if able to click on Discipline Remarks link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eda74b71-41c6-4e03-93ad-7811a5994089</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Attendance and Discipline/Discipline/Verify if able to click on General to view remarks</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1601c99f-36b6-42da-9d90-62cbc4aea7a2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Attendance and Discipline/Discipline/Verify if able to click on Positive remarks</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>99860d27-582c-427a-b631-84c3d8aaf3cd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Attendance and Discipline/Discipline/Verify if able to click on Positive Remarks maximise button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e77c080c-65df-4e18-99a5-6b206aa73026</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Attendance and Discipline/Discipline/Tap on Negative to view remarks</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1073067f-55a6-4e1c-b5b2-838fe0761004</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Attendance and Discipline/Discipline/Verify if able to click on maximise button in negative remarks</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d26c0150-d0dd-4a58-8101-5a243f03963d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Attendance and Discipline/Discipline/Verify if able to click on back button in Discipline Remarks</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>46a9b2aa-2b77-45b4-84a4-10b7d9dbdd40</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Attendance and Discipline/Verify if able to click on back button in Attendance and Discipline page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>04df40c1-4fdb-4bb6-bfa7-97f26685a3cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Facilities/Verify if able to click on Facilities link to view details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>907068bd-16d4-4176-8b4b-ec2f27be1903</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Facilities/Transport/Verify if able to clik on Transport link to view details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>51d0b1ea-d18b-4a50-8109-4383116e8661</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Facilities/Transport/Verify if able to click on Maximise button in pick up details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0537b6fc-1a5c-4da2-b501-0d8b204bd957</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Facilities/Transport/Verify if able to click on minimise button in pickup details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>21bdf1ff-2bec-4636-a675-2ce24d74a8af</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Facilities/Transport/Verify if able to click on Call button in Transport to view manager details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ba0445af-73ae-432c-aa35-b7c4b28f30fa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Facilities/Transport/Verify if able to click on Outside to close the transport manager details dailog</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8f1cdbb4-a6cd-46db-b775-02d0c3929173</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Facilities/Transport/Verify if able to click on back button in Transport to view other menus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3a33228c-8a37-4d6e-8dd8-33f028274342</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Facilities/Verify if able to click on back button in Facilities</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0c727d47-6a1d-4a3f-87cf-e71f80017553</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Documents/Verify if able to click on Documents link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1ea4a98c-7a56-4b9b-a69f-159d74e2e7aa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Documents/Verify if able to check bonafide is visible</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3ec5089b-5a45-4c5d-aaa3-987b633de779</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Documents/Verify if able to click on back button in Documents to view other menus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>135412cf-beae-49c1-830f-0d8131c10537</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Verifiy if able to click on back button in Student details to access all students list</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>01c862c5-f2e8-4dc1-8748-ca804938b4cc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Search/Verify if able to click on back button to view student list</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>31fe05e3-827d-4077-93a3-5947a326094f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Student Details/Verify if able to scroll to Mia Morgan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1c895dbe-2259-4b18-8663-2683c6cf0fd0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Sort By/Verify if able to click on Class-Section Sort by button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>93248790-e389-49fd-becc-df87bc2ef633</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Sort By/Verify if able to click on Name to change the sorting order</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0596ad40-aeea-4690-b4c6-d233bfad51b9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Sort By/Verify if able to click on Name sort by to change sort</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5ea98a29-7bf3-4f93-94e1-22dc51b0be92</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Sort By/Verify if able to click on Admission No to change sorting by Admission no</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>de8ddfe4-fe02-4f97-a385-f4189533981f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Sort By/Verify if able to click on Admission no to change sort by to Roll No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e9704154-9bff-423d-8001-455e03764f34</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Sort By/Verify if able to click on Roll no to check reset</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bd47a39c-c665-4492-b87d-474b8c4c67cd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Sort By/Verify if able to reset the sort by filter to default Class-Section</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b746ee1c-620f-4580-a98a-2d6e5c66acb0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Sort By/Verify if able to click on Name to check close button in sort by</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8866d4e9-06e2-44c0-a458-fb24c3325a13</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Sort By/Verify if able to click on X to close the sort by list</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f95994c0-4ce8-46ca-b7ed-50203952c33e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Filter/Verify if able to click on Filter to see Gender and Clas section details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>13012da5-2d19-45b6-9afd-88b677cc3237</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Filter/Verify if able to select Male in Gender</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>036c5e3d-572c-476a-9d07-49761d5787d8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Filter/Verify if able to select Class from the list</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bc8cd6c5-2270-422d-82cb-0a51e47bb46c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Filter/Verify if able to unselect section from the list</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5ef77b49-fe72-4737-a1d8-9959e44ab47a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Filter/Verify if able to click on Apply button in Filter list</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e58a73f3-c385-4aff-aeee-bbb223789515</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Filter/Verify if able to click on Filter button to reset filter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4011cfd8-4d75-4677-8d1b-68d32190c3f0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Filter/Verify if able to click on Reset button to reset to default list</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eecbe12a-a3a5-4341-a13e-206a8eb568e9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Filter/Verify if able to click on back button after reseting filter preferences</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fe593694-77ef-472e-8041-414fa7062d7f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Filter/Verify if able to click on Apply in Filter popup</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9d70b0e5-6ab6-45e4-a74c-5e6b1344e944</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Search/Verify if able to click on Search button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0cf5776f-3916-45fd-a2e2-929a9ae94366</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Search/Verify if able to enter Admission no for search</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d68c2f3f-78e3-4d89-96d3-7c62ef95129a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Search/Verify if able to click on searched student in the list</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>45767b09-e833-45cf-b5e6-efd1a90940d8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Search/Verify if able to click on See All to view student details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>80bd5898-961c-4371-90c7-d0e86748c76c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Search/Verify if able to click on back button in student details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e1382e35-2b28-4060-aed4-a0d732ea346d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Search/Verify if able to click on back button in student search list</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cbee99f8-e692-4d28-9b15-a7659f6422dd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Student List/Verify if able to click on back button in Student List</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7d14b7f0-41f3-4adc-ad5b-d649a8be8867</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Attendance/Verify Successful access of Attendance links</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fcf4ce2c-dc35-4345-9491-9dd527629ddf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Attendance/My Attendance/Verify Successful access of My Attendance</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>83db09ce-85e0-46c2-9ed0-b9d1aeafa22a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Attendance/My Attendance/Verify scrollable in My Attendance to August-2018</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e47e488a-07c7-4f37-a620-15495f9d263b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Attendance/My Attendance/Verify back button is clickable in My Attendance</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bed35022-a32a-4028-9ba3-17efcf5a1e25</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Attendance/Roll Call/Verify Successful access of Roll Call link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b96dad19-38a7-4da4-b380-0e38a267b3a5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Attendance/Roll Call/Verify Successful selection of first class secton from the populated list</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>873e5be1-d1ca-4561-9f6b-f8adbe528b18</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Attendance/Roll Call/Verify Successful attendance marking for student</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7dcd72df-5f09-4f56-8002-3e52902c619b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Attendance/Roll Call/Verify back button is clickable in Roll Call</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6f0bcac5-31c2-4561-bea0-f3668424a586</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Attendance/Mark/Verify Successful access of Mark link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3c2c09fe-9c3f-437b-83af-8ff4970e0a21</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Attendance/Mark/Verify Successful selection of Class Section</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b7c07e04-2af0-4886-9749-99eadee4861a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Attendance/Mark/Verify Successful student selection for marking Attendance</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>60c94a50-ce2a-4ba6-9c1d-8e0ed4cd0f1d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Attendance/Mark/Verify Successful marking of Attendance</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d2fc9aa5-f33a-44d9-89ef-920ee63f4220</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Attendance/Mark/Verify Successful submit after selecting attendance status</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>877eb87b-d890-4b7a-a1b0-b13f3064ea97</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Attendance/Mark/Verify back button is clickable in Mark Attenance Class Section student list</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>667fd58d-e0ba-4300-a7f2-a523b37b62e5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Attendance/Mark/Verify back button is clickable in Class Section list page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>49e85526-8de3-41bb-85f1-f41444b30a8d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Attendance/Mark/Verify back button is clickable in Attendance</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2d622c80-fdcf-4fd1-8fe0-cd3cd01ed2e9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Calendar/Verify successful access of Calendar links</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0034c233-45d7-44fd-a796-c74ff10d16ef</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Calendar/Verify Day View link is clickable</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f292f33e-90af-4df9-8134-8d1b443201bb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Calendar/Verify Week View link is clickable</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5c708f62-0f58-41d2-87f1-edb3b70975b9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Calendar/Verify Month View is clickable</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4106ed03-520b-4682-888a-13199d89772f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Calendar/Verify back button is clickable in Calendar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>abdc9546-002d-4e7f-b6bc-772957c8b202</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Verify if able to click on Leave to view details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>64fdc142-6533-4a6f-b431-e170d52c7657</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-Apply/Verify Casual Leave is clickable</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8c5935ca-f015-4226-b5ea-245c48949384</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-Apply/Verify back button is clickable in Casual Leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>03c4136c-9073-446e-bb25-6fec92572bc7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-Apply/Verify Compoff is clickable</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6f828fdb-f8f3-47da-bbed-baa91ca3d1dc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-Apply/Verify back button is clickable in Compoff</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>edce512d-3636-481f-9a2f-e15b210bab3a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-Apply/Verify Earned Leave is clickable</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0f684627-2093-4848-95f3-feb53e13b2e0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-Apply/Verify back button is clickable in Earned Leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>58e10469-cced-4f63-875e-e5134bece1d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-Apply/Verify able to scroll to Sick Leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1aa7557f-0a12-4bce-8998-b365fec5e3d9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-Apply/Verify Sick Leave is clickable</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>678911cd-6b71-4c4a-a9d6-a5ab0aeb0fda</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-Apply/Verify able to click on Start Date or End Date</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>20fd9493-e441-434b-b8f7-9b478e97578f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-Apply/Verify able to select Form Date and To Date for applying Leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6b7af643-25bb-44ab-93bc-e285808d08d1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-Apply/Verify able to click on OK button for confirming Start Date and End Date for applying leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1c8ba747-a62d-4e0b-a292-76a7e3de75db</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-Apply/Verify able to click on 2nd Half radio button for applying leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>11588b47-27bf-4620-94ea-6e4ffd7db200</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-Apply/Verify if able to enter reason for applying leave in text box</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>23595c2b-1f62-4e68-bb8c-a1f0c6f1ecb4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-Apply/Verify if able to click on Upload File button for uploading file</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>537122d4-b116-44fc-8702-e15310784efc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-Apply/Verify if able to select file for upload</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5e575b07-2cd8-4b7d-a533-5d10430baadb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-Apply/Verify able to click on Apply button for submitting sick leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>365349f9-d80c-475d-b399-54025b30a623</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-Approvals/Verify able to click on Approvals tab</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c6d749df-4244-4cad-9083-d173375f7940</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-Approvals/Verify able to click on Student button for Approvals</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1c516dc3-4493-4583-a8bf-ee1b27dc3011</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-Approvals/Verify able to click on Approve for approving student leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b68002d1-66fd-4b7c-994c-f2645e655bb0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-Approvals/Verify able to enter remark for approval after selecting Approve</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bc5e496d-c555-4359-84ba-b91a27d64a0b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-Approvals/Verify able to submit after enetering remark for student leave approval</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>01f102ed-abe1-46c5-a9be-6adf82962f63</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-Approvals/Verify back button is clickable in Leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ff9cc0b0-d1ba-463b-a4ea-d303fbdde41f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-History/Verify able to click on History icon</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>87234ce7-0c22-4717-83b1-5b75eb8a5f28</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-History/Leave-History-All/Verify able to click on All button in History</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a58a6f42-5e88-4b77-922a-b37c6bfe2574</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-History/Leave-History-All/Verify able to click on leave to view details in all</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6b2d19cd-f8d6-4d9d-8449-a1d2040566d4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-History/Leave-History-All/Verify able to click on Withdraw button after selecting leave in all</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c4153d7a-27b9-45bc-9bac-9cbd0e70c75a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-History/Leave-History-All/Verify able to enter remark for reject reason in all</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9cf7cccf-69d0-4352-bd21-3aef7bbf5d39</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-History/Leave-History-All/Verify able to click on Done button after entering reason for rejection</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2da2c789-5ea2-4ccc-907f-1cc2dfbb7e9b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-History/Leave-History-Pending/Verify if able to click on Pending button in Leave History</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e278f5a8-96d7-4f17-994c-af5561b0073e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-History/Leave-History-Pending/Verify if able to click on Sick Leave in pending</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>77d746b2-4b84-4240-b0c8-04c088e031ef</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-History/Leave-History-Pending/Verify if able to click on Withdraw button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>03b933bf-42aa-4dc8-b3c0-d88432d68b31</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-History/Leave-History-Pending/Verify if able to enter remark for withdrawal in pending</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>959d788e-5587-4b60-8973-27066c78c0dd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-History/Leave-History-Pending/Verify if able to click on Done after entering reason for rejection in pending</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>db1b8e7e-2de2-4ddf-a746-97f4e70c19c2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-History/Leave-History-Approved/Verify if able to click on Approved button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9dcb736f-b470-427e-ac61-cd1b061eb405</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-History/Leave-History-Approved/Verify if able to click on Approved leave to view details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>460995ab-b199-4806-a695-a439b6b3990b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-History/Leave-History-Approved/Verify back button is clickable in Approved leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d963f940-656f-4c07-91c6-66e8c8b40342</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-History/Leave-History-Rejected/Verify if able to click on Rejected button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>749e16d0-0902-486b-8839-f605e4c5b9e5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-History/Leave-History-Rejected/Verify if able to click on leave to view details in rejected tab</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5be08dda-25a4-42a1-b6ea-35ad82cadd7a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-History/Leave-History-Rejected/Verify back button is clickable in selected leave under rejected tab</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8b567dc8-8dd3-4f86-b09f-19e9be067d01</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-History/Leave-History-Rejected/Verify if able to click on back button in leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6aaf4b81-a383-4e0b-90ca-393b6d4b0f29</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-History/Leave-History-Cancelled/Verify if able to click on Cancel button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5f0fe6f5-4480-4884-9eb2-b413979c1b21</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-History/Leave-History-Cancelled/Verify if able to click on cancelled leave to view details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>72c0c249-bd4e-4a83-8042-51f3ffea78bf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Leave-History/Leave-History-Cancelled/Verify back button is clickable in cancelled leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8e0d62f3-4c37-45d2-b673-89142e1285f8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin Login/Leave/Verify if able to click on back button in History page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3cb1dc22-d3e0-44cd-8e51-6d95ba7a0c2b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Transport for Admin Staff/Verify successful access of Transport links</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6e34f311-d29e-4a67-b095-476083c40af1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Transport for Admin Staff/Verify expand button is clickable to view details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e2523159-2474-4e46-abd3-fcbba92aff43</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Transport for Admin Staff/Verify able to click on Minimise button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>331d1697-8efd-40f4-8762-1ee25a420183</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Transport for Admin Staff/Verify able to click on Transport Call button to view details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ed0c0b69-98c5-4ef2-aabe-b5a8db7db62c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Transport for Admin Staff/Verify able to click on outside dialog to close the telephone details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cf24417f-d174-4bb0-91f5-ae95f5f45562</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Admin Login/Transport for Admin Staff/Verify back button is clickable in transport</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
