<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Parent Execution</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>9432bc78-f904-4b9f-b474-43d37f6180ef</testSuiteGuid>
   <testCaseLink>
      <guid>6200001d-6147-4a4d-8160-0f93b22401be</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Parent Login/Login/Verify if able to launch the application successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8787a448-7d2f-46a6-878d-d677fd5e7b47</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Login/Verify if able to click on FAQ to view details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0c89a721-8c67-4031-b472-f1854bf86c2d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Login/Verify if able to click on back button in FAQ</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6a315bbf-0603-4c07-92e3-9013e1edd836</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Login/Verify if able to click on Dont have School Code or Domain link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7e89afb8-9766-474c-91df-bd7301ec5d17</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Login/Verify if able to click on OK button to close dialog</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>057bc642-3ffa-45a7-988d-0aeb4879e6a4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Login/Verify if toast message is displayed when invalid domain is entered and clicked on proceed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9cd66938-d4bd-4af2-9e27-6f9d9737ff4a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Parent Login/Login/Verify if able to enter school code</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0213b190-8535-493f-8c95-cc47f9d92ef1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Parent Login/Login/Verify if able see G Login screen after clicking on proceed with valid school code</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0ec1f4fd-7b34-444c-9c60-cf8fd055e264</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Google Login selection/Verify if able to click on edit school code or domain button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>46f60ee6-11e9-424b-a4b2-26a96f88d9d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Google Login selection/Verify if school code screen is shown when clicked on edit school code or domain</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>09462de7-3954-488e-aa05-ccb727c02dc7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Login/Resuse School code and proceed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c9f1158f-64ec-4cdb-b675-ede23fde9f0e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Parent Login/Google Login selection/Verify if able to sucessfully click on Login with Google button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4e27216b-a122-4dcd-96ae-0c4c001e46d1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Parent Login/Google Login selection/Verify if able to choose parent email id from the list</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>da36d423-285a-4d91-b7fa-986b19a449fe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Menu/Home/Verify if able to click on Menu after logged in</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f7544007-928e-4f4e-8f2c-07ec5399f03f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Menu/Home/Verify if able to click on Menu after logged in</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ae6e0e16-97e6-4d37-bcb0-640b9c0cd42b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Menu/Home/Verify if Student details, Home, Settings, Magazine and logout are displayed or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e2df3246-e996-4b69-8e4c-6723bc0eb1d8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Menu/Home/Verify when clicked on Home, Main menu details are showing are showing or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fc8169e6-9f36-421f-ab52-6ae79996b318</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Parent Login/Login/Push Notifications/Verify if able to click on Yes in the Push Notifications popup screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2d851389-6b66-4a75-a672-4c02d0aeaaf1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Menu/Settings/Verify when clicked on Settings Push notifications details are shown or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>781f6c6b-0809-4287-b3da-aedec8a5f25e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Menu/Settings/Verify when clicked on Recive Notifications toggle, push notification popup is showing or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3e1ca772-73aa-4681-94a6-3dcec488989f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Menu/Settings/Verify when clicked on NO in push notification is the toggle status getting updated or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e50773a0-575a-4b47-a4da-ba544e358c4d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Menu/Settings/Verify when clicked on Recive Notifications toggle, push notification popup is showing or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>81c55b69-3a22-4032-92b0-ab4655146cee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Menu/Settings/Verify when clicked on YES in push notification is the toggle status getting updated or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>994ac14d-eda9-4af4-9608-8b9e2cbf53bd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Menu/Home/Verify if able to click on Menu after logged in</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9f4a7e9c-ea5c-4294-bf0e-e385ab15be3f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Menu/Magazine/Verify when clicked on Magazine, Magazines page is getting launched or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>03b908a6-c982-45cd-a4c6-476918999fa6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Menu/Magazine/Verify if first magazine is clickable or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9314d3c7-ea56-4c72-acb8-5114c71a538b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Menu/Magazine/Verify if able to successfully click on Download PDF button for downloading magazine</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2bc9a452-5b5f-4bc2-9f0a-1ca4c2c748ab</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Menu/Magazine/Verify if downloading of magazine is started or not from notification panel</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1e25f16a-c5b1-4f66-b45e-64c8f295a95f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Menu/Magazine/Verify if able to click on close button to close the magazine preview</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6ffcf525-5d5e-412f-8fb5-712c949768ee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Menu/Magazine/Verify if second magazine is clickable or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9fa96922-b248-4594-a1f6-3ee46c88e575</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Menu/Magazine/Verify if able to click on View button to view the downloaded magazine or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3812ad23-ed4f-426a-b642-2d76791384eb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Menu/Magazine/Verify if able to click on close button to close the magazine preview</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>af3ffc23-7a9f-448c-8ce9-70e27ad16ead</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Menu/Home/Verify if able to click on Menu after logged in</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9f518ca4-d07e-4711-853e-51179bed4ee4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Menu/Home/Logout/Verify when clicked on Logout application is getting logout out or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8aa43f0a-91c7-4855-8c22-a563e14abedc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Login/Verify if able to enter school code</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9d913994-e36d-4f13-a1b0-94fd77c067ea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Login/Verify if able see G Login screen after clicking on proceed with valid school code</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>128b1da2-c76e-4fdc-bd3e-b08e0f2a128d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Google Login selection/Verify if able to sucessfully click on Login with Google button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>76f35901-a71a-4ca6-a16f-acd4f04e61af</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Google Login selection/Verify if able to choose parent email id from the list</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bf6359cd-4042-479b-b852-83245581d837</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Login/Push Notifications/Verify if able to click on Yes in the Push Notifications popup screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>30fe3037-63bb-4e42-bb52-5cec61024d89</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Menu/Bell Notification/Verify if able to click on Bell icon to view notifications</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f5326145-b2fa-4a7c-81a1-464df3869ecf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Menu/Bell Notification/Verify if able to click on back button in Notifications page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b16c734b-1b0e-466e-9d96-90095febf96d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Student Profile/Verify Student profile is clickable or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>319d8072-4633-442c-afb2-a61e47f2b5a3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Student Profile/Student/Verify if student is clickable or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bc4cfb72-14bb-46d1-9939-f53e2467123e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Student Profile/Student/Personal Details/Verify if personal details is getting minimized or not when clicked on minimize button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>162ba03c-217c-426a-8924-72c938fcc7b8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Student Profile/Student/Academic Details/Verify if able to view Academic details or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a3ded218-9ff9-4e47-95a0-64f21dd548d4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Student Profile/Student/Academic Details/Verify if able to click on Minimise button to close details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4a15d11d-659a-45f7-82e8-bb8e7ff9aebc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Student Profile/Student/Contact Details/Verify if able to view Contact details or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>db4207db-5118-40fa-80a4-59b1eb8f1596</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Student Profile/Student/Contact Details/Verify if able click on Mimimise button to close details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1ef98ba5-1dcc-4103-8931-3e9dbc1d668f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Student Profile/Student/Previous School Details/Verify if able to view Previous School Details or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>56248bc3-a1bc-4cee-80a2-1aa92728a2f2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Student Profile/Student/Previous School Details/Verify if able to click on minimise button to close details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>268bcf7b-761c-4c8b-9407-682d0ae385c0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Student Profile/Student/Miscellaneous Details/Verify if able to view Miscellaneous Details or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>52112d0d-808b-4b13-91b0-bd4fc6e739f9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Student Profile/Student/Miscellaneous Details/Verify if able to click on Mimimise button to close details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d71fa603-6644-4608-8e9c-62cfe50b807d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Student Profile/Student/Verify if able to click on back button in Student Details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>faa1ae0e-7643-4f62-9db9-be24b26864b5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Student Profile/Parents/Verify if able to click on Parents tab to view details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d27c73e2-7c9a-4913-b1aa-19da8c389820</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Student Profile/Parents/Veify if back button is clickable to access other menus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5d3457c1-5b1f-47bd-be5c-a9087c5f72dc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Student Profile/Guardians/Verify if Guardians menu is clickable or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>66425c64-016a-4146-bd43-d8751dbebaa4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Student Profile/Guardians/Verify if able to scroll to view Guardian Details or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>256dae80-1533-4771-88de-2af018538ea5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Student Profile/Guardians/Verify if able to click on back button to view other menus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d3c9dca8-ab68-40f5-85a7-18b58a7c9012</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Student Profile/Documents/Verify if able to click on Documents menu or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>65ce4056-324b-4afa-a598-2b9f9d894ed0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Student Profile/Documents/Verify if able to click on download button to download the doc</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3e828598-5d42-4aa2-98a5-f87545357e04</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Student Profile/Documents/Verify if able to view the downloaded document or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>38960645-3f54-46f1-a2e2-7a7151a40b6a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Student Profile/Documents/Verify if able to click on back button to view other menus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>87d55849-f1a9-4d21-b2e0-a93a8e6c4983</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Attendance/Verify if Attendance Menu is clickable or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>897d18aa-3497-49c0-83ad-0907835190cd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Attendance/Verify if able to view all leave types of attendance</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8742300b-e2a7-449d-be2d-a4ae62df75e7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Attendance/Verify if able to click on back button to view other months details or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2b8e9b14-5933-426c-a598-f5f2b86eef7d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Attendance/Verify if able to view attendance status for the selected date</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0c4cf862-b3ef-4472-aa1d-7b18ada89160</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Attendance/Verify if able to click on attendance status for other day status</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>441b41bd-aedf-481e-9774-7045a1c78d58</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Attendance/Verify if able to click on Next button to view other months</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0937dfdc-b2e8-41ad-bce8-d388cfb3b380</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Attendance/History Button/Verify if able to click on Attendance History button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>15e7d16d-ae1e-4ae2-a686-bc09cbee510c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Attendance/History Button/Verify if able to click on month to view attendance</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>38f49dc3-7af7-4338-8388-1bb9c5bfb849</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Attendance/History Button/Verify if able to view attendance status of the selected date</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ee7cdc93-7a93-4953-bd79-32ac0d0051a2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Attendance/History Button/Verify if able to view attendance types</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7f80bc2a-7ce0-4c83-990a-745325b16d77</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Attendance/History Button/Verify if able to view next month attendance</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a16c4d2b-edc2-42c2-ba0d-b4b04f5ae551</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Attendance/History Button/Verify if able to view back button for the other months</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fcfbf7f6-e809-4213-8cba-e7c29694626b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Leave/Verify if able to click on Leave Menu or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a5dfe14c-d460-486a-9e0b-937d3cbd23f0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Leave/Verifiy if able to click on Applied tab to view applied leaves</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b779c50e-8f9a-41a9-a23c-7ff33dfd571f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Leave/Verifiy if able to click on Confirmed tab to view confirmed leaves</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>478656cc-7629-4736-80bd-84c7be05ebd3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Leave/Confirmed/Verify if able to view details when confirmed leave is selected</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>09eaa3b9-d957-4985-97cf-96cbbd0aaa55</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Leave/Confirmed/Verify if able to click on Withdraw Leave request button to withdraw</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>58dac10a-f035-43ca-9ab7-5c1e0d712183</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Leave/Verifiy if able to click on Rejected tab to view Rejected leaves</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8b5c29d7-32d8-4c89-a3b7-e6ca233f5b8b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Leave/Rejected/Verify if able to view Rejected leave details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3165e173-00ec-4016-92ba-0d1fc15b9dc0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Leave/Rejected/Verify if able to click on back button to access leave details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e11678d4-7ca6-444b-98e8-2811025db918</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Leave/Apply Leave/Verify if able to click on Add button to add leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d5096f13-edc8-4aa9-99c8-afc72555ea03</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Leave/Apply Leave/Verify if able to click on a date to apply leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fcbf3d3b-3419-4ebb-807c-5edfd4cd9161</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Leave/Apply Leave/Verify if able to click on OK to apply leave</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>da833e38-f171-4a10-9e60-6871980af855</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Leave/Apply Leave/Proceed Folder/Verify if able to click on First Half tab</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e06962f3-4057-49e0-b5db-aa09c06e93f3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Leave/Apply Leave/Proceed Folder/Verify if able to click on Proceed button after selecting Leave type</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>84f3697f-536a-4d84-b1ec-11c0a07aea68</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Leave/Apply Leave/Subject and Reason Leave/Verify if able to enter Subject for applying leave or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c69d9695-399f-4f50-91ce-3ca6919ebe55</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Leave/Apply Leave/Subject and Reason Leave/Verify if able to enter Reason text</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e29958fa-1f9c-4e77-a161-69647625d530</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Leave/Apply Leave/Subject and Reason Leave/Verify if able to click on proceed after entering subject and reason</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4db6c77b-64db-4530-9b83-0edb9361ca91</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Leave/Verifiy if able to click on Applied tab to view applied leaves</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c0ef9dc8-7027-47b8-b8b3-598927017d1d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Leave/Applied/Verify if able to view details when applied leave is selected</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6b458f4f-ff7b-40c0-b485-4015aa8c5a30</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Leave/Applied/Verify if able to click on Withdraw Leave Request button to cancel</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4a151e1f-28ec-4c64-9082-18544626d16c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Leave/Verify if able to click on back button to view other menus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a99799ff-6a0d-4006-baea-33caccb3e0ae</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Verify if able to click on Fee tab to view Fee details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d00e5627-39b2-478a-a10b-7bb04c08096b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Payments/Verify if able to click on Fee Payments tab to view details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c4929fca-f748-4174-ae78-f7b9101dec9d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Payments/Total Fee/Verify if able to click on View Details to check Total Fee break up</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f3ef6dae-c0fd-4e1a-a6d3-534c32f4572b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Payments/Total Fee/Verify if able to click on View more to view details of First Fee</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>36c06eec-ae95-4663-b46d-d9d8ca3aee94</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Payments/Total Fee/Verify if able to click on back button in Total Fee details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>df7e1111-d0c0-4226-815e-112ae1cbdd97</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Payments/Total Fee/Verify if able to click on back button in Total Fee details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bb9c75c2-c071-404a-8b45-d2a2c9fd5eda</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Payments/Paid till date/Verify if able to click on View details tab to view paid details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8bce3b77-00fe-48db-a825-07cfd61b98f3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Payments/Paid till date/Verify if able to click on view details to check Paid till date breakup</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>01eed49b-a8ea-4eb1-81af-a19a7e302bd9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Payments/Paid till date/Verify if able to click on back button to view Payments done till date screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0ea28650-763a-4597-b58c-39ca3e572ac7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Payments/Paid till date/Verify if able to click on download to view payment receipt in pdf</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>86d269b0-c96b-491d-b245-89ed6506631c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Payments/Paid till date/Verify if able to click on close button in Receipt Details page or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>63a1ca75-2892-4199-91bf-a391a39ddb49</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Payments/All Due/Verify if able to click on Pay Now in all due to pay dues</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>daa69bdb-7e4a-4089-b98e-39d2baf5cf5b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Payments/All Due/Verify if able to click on Maximise button to view Fee total breakup</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2d377435-467d-4de6-9c38-ae5c9dfcb348</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Payments/All Due/Verify if able to click on Minimise button to close the Fee breakup</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dcfbec61-b62c-4b96-86eb-d999ea255528</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Payments/All Due/Verify if able to click on Confirm button to proceed for payment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4032b4ed-b2f9-4b04-b06b-c80052b4a630</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Payments/Payment Summary/Verify if able to click on Terms and Conditions hyperlink to view details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6d70f692-a7e1-4eca-8bda-e28d1067225c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Payments/Payment Summary/Verify if able to scroll in Terms and Conditions</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>edea1eb8-6c16-43c4-84f1-ac521bbd0926</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Payments/Payment Summary/Verify if able to click on back button in Terms and Conditions</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>29a0a646-a676-49ab-887e-df0d7be4dc58</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Payments/Payment Summary/Verify when clicked on cancel is navigating back to All Due page or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9c88147b-f4f5-45eb-8809-7bbe7807d116</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Payments/All Due/Verify if able to click on Confirm button to proceed for payment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6966e5f0-e000-40f9-ac32-d96fbdfa507f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Payments/Payment Summary/Verify if able to click on check box to agree Terms and conditions</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7732e6dd-864c-4315-8f02-8c7bf5828379</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Payments/Payment Summary/Verify if able to click on Make Button to proceed for payment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d2e26331-e7e6-4180-a527-2f7e60901fcb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Payments/Razor Pay/Verify if able click on Phone no to edit</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>697bac30-f780-4377-9bb1-bfb624f24d54</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Payments/Razor Pay/Verify if able to click on Email to edit</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e563f0a2-4d43-4c82-aacb-a185af143ba7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Payments/Razor Pay/Card/Verify if able to select Card as payment mode</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0b35d382-b7c5-4442-8758-9b461bdad69c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Challan/Verify if able to click on Fee Challan to view challan generated so far or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>931abe7b-e28a-4626-b5ed-537cbafeb651</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Challan/Verify if able to click on Challan download to view generated challan details or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>32c1338e-0473-43fe-bba5-37b6db692d7c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Challan/Verify if able to click on Filter to view Academic Sessions or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bb589f6a-2094-48f7-8b24-e1a7fab1f275</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Challan/Verify when clicked on Academic session is it showing available academic sessions or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f5eec53b-c095-4b55-a870-6deeb4396f2d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Challan/Verify if able to click on First Academic session to view details or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8fcc52e8-1a7a-413c-a02c-5e784bef38ae</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Challan/Verify if able to click on Apply button for viewing selected academic session details or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a48385ff-6976-4ebb-84e0-73e89c52aee4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Challan/Verify if able to click on Filter to view Academic Sessions or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b9483500-d8f4-4fa2-8553-f3a37064a076</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Challan/Verify when clicked on Academic session is it showing available academic sessions or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>08cd04cb-136f-4c8b-9336-3daab6998201</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Challan/Verify if able to select other academic year for viewing challan details or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>06b01163-cf81-440d-a65a-8d67f2f2f574</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Challan/Verify if able to click on Apply button for viewing selected academic session details or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2f26874f-3b51-49f3-b902-0ba4e4b572b5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Challan/Verify if able to click on Filter to view Academic Sessions or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9a3cf92c-f589-48bb-88e9-cb8cdb0df9c1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Challan/Verify if able to reset the Academic session to current academic session or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5dd53fe7-257c-4888-b843-2b08df5bc57e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Challan/Verify if able to click on back button in Fee Challan generated page or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4ef97a31-03a0-4175-9b11-a1d74b5f18bb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Transport/Verify if able to click on Transport to view details or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ed191e61-8e3e-41e8-aee7-8b435a0959d7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Transport/Verify if Maximise button is clickable or not in Pick Up details to view Driver and attendent details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>387dd16e-d08b-423b-9d68-064da861ac74</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Transport/Verify if able to click on Minimise button to hide the Pickup details or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6516dd8e-8a54-425c-a9aa-95e579d5218f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Transport/Verify if able to click on Maximise button to view drop details or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bf78851b-4a7e-49d9-9953-3bf55c2f37f3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Transport/Verify if able to click on Minimise button to hide the drop details or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1c8740ba-fea0-4ace-aea3-8733c1c4caa6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Transport/Verify when clicked on call button Transport manager details are displayed or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>317df8d2-ebe6-4ae8-929d-bd34dccaebe2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Transport/Verify when clicked on Mobile no is it launching dailpad or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>394c43b3-fa9b-4774-9fbc-75a3e92937b5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Transport/Verify if able to click on back button in transport details to view main menu</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e05d843f-032c-41b2-ba62-5209eac7ed64</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Examination/Verify Examination is clickable or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7de4b2d9-d77d-45da-9c13-b794ba90ab55</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Examination/Verify when clicked on Report Card is it showing report cards to download or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d4f1dbd0-6f27-485c-82a3-43a3aec5e381</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Examination/Verify if able to download Report card or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0e84c252-d278-43b8-9f78-98784c37bc7c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Examination/Verify if able to click on back button in Report Cards to view Examination page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0e61ade4-07a7-4164-a87a-7a27bdd7e046</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Examination/Verify if able to click on back button in examination to view main menu or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>91e35b05-a239-46f5-b00d-621668d7a07e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Calendar/Verify if Calendar is clickable or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b381e317-4e03-415e-b99a-a9db700ece6c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Calendar/Verify if Week View is clickable or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dc607def-498b-421d-af46-0dbcca432a6c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Calendar/Verify if Month View is clickable or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7cc4de19-4192-4cba-b0ac-2968ee011960</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Calendar/Verify if able to select date to view event details in month view or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>80c76d16-b120-4ba0-8525-a4e2514776b5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Calendar/Verify if able to click on back button in Calnder to view main menu or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>baaf78fa-43e7-4633-9fcd-27cfd9115b59</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Calendar/Verify if Calendar is clickable or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>43f2535a-16b5-4ed7-a6b4-9b0428a16943</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Calendar/Verify if Month View is clickable or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8866255f-960c-4d0d-ba02-dcb63132dc87</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Calendar/Verify if able to select date to view event details in month view or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b815f4ec-be3a-4382-be33-55c0d4920e1a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Calendar/Verify if Day view is clickable or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e1cac5b8-1ada-4951-8e52-a358692d4898</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Calendar/Verify if able to click on Filter to view details or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4f16cf34-1123-4fde-a915-ef13b71ac465</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Calendar/Verify if able to select Sports Events or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>70839e9c-bc78-4ee4-8da0-3075565ad2e9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Calendar/Verify if able to click on back button in Calnder to view main menu or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6712f728-466e-463d-a7a9-eb7d93a2e631</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Messages/Verify if Messages is clickable or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0f5c7dfb-ec68-4f80-bb4f-969df3b2893d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Messages/Verify if SMS is clickable or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>479d3f4b-2456-42b8-bb2d-5d40583fbe74</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Messages/Verify if able to view SMS details when clicked on Message</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2b2312a1-22fa-4d30-946c-735ed791884a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Messages/Verify if able to click on back button in SMS details page or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4da31d0c-92e6-44c1-82ec-1a83ec6908ff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Messages/Verify if EMAIL is clickable or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b3114873-cc6d-4285-8863-aac711eb799f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Messages/Verify if able to click on Email to view details or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>be25274f-cca9-4c04-8b67-51ce08476ae1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Messages/Verify if able to click on download to view the email attachment or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e7db42cb-5b86-4b5a-959c-a0caa7df1116</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Messages/Verify if able to click on back button in Email details page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c056dc68-e636-4a40-bc6a-8dd2597ae4c0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Messages/Verify if NOTIFICATION is clickable or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>af82f5da-a025-4e17-8d3c-d0b826123d6c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Messages/Verify if able to click on Notification to view details or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9e9b467f-1346-40b4-a5e2-e116b3cabe02</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Messages/Verify if able to download notification attachment or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2379327e-17a2-41f8-8c35-cf684f2247d0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Messages/Verify if able to click on back button in Notification view details or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e300ba2b-a47f-4008-bfcf-e1661180ecd8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Messages/Verify if able to click on Filter to view details or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>209fa25e-2049-4e74-9012-20d6b66b64f7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Messages/Verify if able to click on Today in filter list</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>03cbe5a9-2761-401a-ac26-c789d8c29d3c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Messages/Verify if able to click on Filter to view details or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8a294cb9-5194-4da7-9e1c-010ceb3c4a77</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Messages/Verify if able to click on Last 7 Days in filter list</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7aed20fd-b505-4ec0-b3d0-11053827d8bd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Messages/Verify if able to click on Filter to view details or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4f17eac2-590c-4987-a87e-4b2dcac7697e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Messages/Verify if abel to click on Last 15 days in filter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d9755991-3e6f-4247-bdcb-c43c5e591514</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Messages/Verify if able to click on Filter to view details or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d394cab6-fe76-4ba5-91fa-5d928320ba1f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Messages/Verify if able to click on Last 30 days in filter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>972bd7a8-2a9c-432e-abe5-5fb5863ad1ee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Messages/Verify if able to click on Filter to view details or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2a18b2dc-bcd7-49a8-9f86-bb127592f629</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Messages/Verify if able click on Custom Date range in filter list</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>82523429-d7d3-4f05-ab7c-49b1857a65e0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Messages/Verify if From date is selected in Custom date range</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bcbe18ea-10b5-431c-a63d-8f5df0ddd8c8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Messages/Verify if able to click on To date from Custom date range</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2b279394-21c6-4ce5-9a44-2abbe7e4b13f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Messages/Verify if able to click on Apply Button after selecting from and to date range</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>78ac0399-2dc8-4c1f-b432-337a1dfde01e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Messages/Verify if able to click on Filter to view details or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>849e489a-2f5a-479f-9727-5faa16abe1d7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Messages/Verify if able to click on All Messages in filter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b73155c1-9450-48cd-8266-5cd8b7277cab</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Certificate/Verify if able to click on Fee Certificate to view details or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>44c1c0b4-8531-4566-b6c1-671132bf71c1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Certificate/Verify if able to view paid fee certificate when clicked on download button or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c7f1233a-8503-47aa-96f0-f68875cd55c2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Certificate/Verify if able to click on Search button to search for specific Certificate or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>350b4987-dd93-42eb-9c3a-64eaa1de877a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Certificate/Verify if able to search enterd Certificate no to view details or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b1f017a8-14bf-4771-972e-fcdb7c92d945</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Certificate/Verify if able to click on back button after searching the certificate no or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e0c41a0f-0f7c-4e4a-b982-72a5ea54265d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Certificate/Verify if able to click on Filter to view Academic Sessions or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>89ac0c63-0c91-4fe8-a4cd-4d764fab31c7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Certificate/Verify if able to click on First Academic session to view details or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b24653c5-0e40-4565-a1fc-ce0e4ddbba8f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Certificate/Verify if able to click on Apply button for viewing selected academic session details or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>21d49d4b-8d14-42f4-bf0b-a96a8dd82549</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Certificate/Verify if able to click on Filter to view Academic Sessions or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6cf2a183-2271-41fe-b9b9-2ab1daa59a6c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Certificate/Verify if able to reset the Academic session to current academic session or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>81546ba3-380d-48d6-ab85-5714e5e4e21f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Certificate/Verify if able to click on Apply button for viewing selected academic session details or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>924c919f-c7bd-4bc4-b9d6-43307e6ef888</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Certificate/Verify if able to click on Filter to view Academic Sessions or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>203cec11-e186-4ddb-8947-9d3b11289304</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Certificate/verify if able to click on back button in Fee Certificate details page or not</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>35f75be4-d75e-4dda-be30-46f57d1d055b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent Login/Main Menu after Login/Fee/Fee Certificate/Verify if able to select other academic year for viewing challan details or not</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
